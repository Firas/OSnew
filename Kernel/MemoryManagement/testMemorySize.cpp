
#include <iostream>

#include "MemorySize.hpp"

char buffer[8*64+1];
int main(){
	MemorySize size0(1024);
	MemorySize size1(512, 1024);
	
	std::cout << size0.usedSize.toString((char*)buffer) << ' ';
	std::cout << size0.capacity.toString((char*)buffer) << std::endl;
	
	std::cout << size1.usedSize.toString((char*)buffer) << ' ';
	std::cout << size1.capacity.toString((char*)buffer) << std::endl;
	
	return 0;
}
