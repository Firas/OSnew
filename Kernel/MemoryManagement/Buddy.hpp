
#ifndef	_BUDDY_H_
#define	_BUDDY_H_

#include <DataStructure/Basic/UnsignedInteger.cpp>
#include <DataStructure/SimpleLinkedList/LinkedList.hpp>
#include <DataStructure/SimpleLinkedList/LinkedQueue2.hpp>

#include <ArchitectureDependent/x86/DataStructure/Synchronization/SpinLock.cpp>

// TODO: 记录一个获取了的内存区域的大小和容量（2的指数）的数据结构应放在哪里？

/**
 * 对于Intel/AMD的x86/x64架构，最小的页面为4KB
 * 1GB = 4KB * 2^18，所以MAX_ORDER可定为19
 */
/**
 * For Intel/AMD x86/x64 architecture, the smallest page is 4KB
 * 1GB = 4KB * 2^18, so MAX_ORDER can be 19
 */
template <unsigned BYTES_PAGE, UINT8 MAX_ORDER> class Buddy{
public:
	SpinLock lock;
	UnsignedInteger numPages;
	// TODO: pointer to bitmap of free/busy
	CALLCONV bool (*isFree)(void *addr);
	CALLCONV bool (*setBusy)(void *addr, UnsignedInteger numPages);
	CALLCONV bool (*setFree)(void *addr, UnsignedInteger numPages);

	LinkedQueue2<UINT8> emptyAreas[MAX_ORDER+1];

	CALLCONV void initialize(CALLCONV bool(*isFree)(void*),
			CALLCONV bool(*setBusy)(void*, UnsignedInteger),
			CALLCONV bool(*setFree)(void*, UnsignedInteger)){
		lock.release();
		numPages = UnsignedInteger(1 << MAX_ORDER);
		this->isFree = isFree;
		this->setBusy = setBusy;
		this->setFree = setFree;
	}
	
	/**
	 * 伙伴系统之获取页面算法：
	 * 1、找到合适的指数
	 * 2、到该指数对应的链队列中寻找内存区域
	 * 3、若该链队列不为空，取出一个元素，并返回
	 * 4、若该链队列为空，访问更大的指数对应的链队列，
	 *  　直到链队列不为空
	 * 5、从大指数对应的链队列中取出一个元素，将该内存区域
	 *    二等分放到小指数对应的链队列，直到第3步可以完成
	 */
	CALLCONV void* getPages(UnsignedInteger number, bool continuous=false){
		void *returnPage = (void*)0;
		UINT8 exponent;
		UnsignedInteger power(1);
		if(number > numPages) return (void*)0;
		
		lock.get();
		
		for(exponent = 0;
			exponent <= MAX_ORDER && power < number;
			exponent++, power <<= 1);
		if(emptyAreas[exponent].isEmpty()){
			if(exponent >= MAX_ORDER){	// TODO: change condition
				if(continuous) return (void*)0;
				// TODO: apply for discontinuous areas
			}
			UnsignedInteger tempPower = power;
			for(UINT8 tempExponent = exponent + 1;
					tempExponent <= MAX_ORDER;
					tempExponent++, tempPower <<= 1){
				if(emptyAreas[tempExponent].isEmpty() == false){
					emptyAreas[tempExponent].removeFirstNode(
							reinterpret_cast<LinkedList2Node<UINT8>**>(&returnPage));
					// 这里返回大内存区域的前面部分，后面部分插入空闲链表
					for(UINT8 i=tempExponent; i>exponent; tempPower >>= 1){
						UINT8 *tempPointer = reinterpret_cast<UINT8*>(returnPage);
						LinkedList2Node<UINT8> *node =
							reinterpret_cast<LinkedList2Node<UINT8>*>(
								tempPointer + (BYTES_PAGE*tempPower.value));
						node->initialize(--i);
						emptyAreas[i].addNodeToLast(node);
					}
					break;
				}
			}
			
		}else{
			emptyAreas[exponent].removeFirstNode(
					reinterpret_cast<LinkedList2Node<UINT8>**>(&returnPage));
		}
		if(!setBusy(returnPage, power)){
			// TODO: deal with failure
		}
		numPages -= power;
		
		lock.release();
		
		return returnPage;
		
	}

	/**
	 * 伙伴系统之返还页面算法：
	 * 1、
	 * 2、
	 * 
	 */
	CALLCONV void returnPages(void *addr, UINT8 order){
		UnsignedInteger power(1 << order);
		LinkedList2Node<UINT8> *nodeToMerge;
		
		lock.get();
		
		setFree(addr, power);
		
		while(order < MAX_ORDER){
			if(isFree(reinterpret_cast<void*>(
					reinterpret_cast<UINT8*>(addr)-(BYTES_PAGE*power.value)
					))){
				nodeToMerge = reinterpret_cast<LinkedList2Node<UINT8>*>(
						reinterpret_cast<UINT8*>(addr)-(BYTES_PAGE*power.value));
				if(nodeToMerge->key == order){
					{	// remove nodeToMerge from emptyAreas[order]
						if(nodeToMerge->prev != (LinkedList2Node<UINT8>*)0){
							nodeToMerge->prev->next = nodeToMerge->next;
						}else{
							emptyAreas[order].head = nodeToMerge->next;
						}
						if(nodeToMerge->next != (LinkedList2Node<UINT8>*)0){
							nodeToMerge->next->prev = nodeToMerge->next;
						}else{
							emptyAreas[order].end = nodeToMerge->prev;
						}
					}
					addr = reinterpret_cast<void*>(nodeToMerge);
					++order;
					power <<= 1;
					continue;
				}
			}
			if(isFree(reinterpret_cast<void*>(
					reinterpret_cast<UINT8*>(addr)+(BYTES_PAGE*power.value)
					))){
				nodeToMerge = reinterpret_cast<LinkedList2Node<UINT8>*>(
						reinterpret_cast<UINT8*>(addr)+(BYTES_PAGE*power.value));
				if(nodeToMerge->key == order){
					{	// remove nodeToMerge from emptyAreas[order]
						if(nodeToMerge->prev != (LinkedList2Node<UINT8>*)0){
							nodeToMerge->prev->next = nodeToMerge->next;
						}else{
							emptyAreas[order].head = nodeToMerge->next;
						}
						if(nodeToMerge->next != (LinkedList2Node<UINT8>*)0){
							nodeToMerge->next->prev = nodeToMerge->next;
						}else{
							emptyAreas[order].end = nodeToMerge->prev;
						}
					}
					++order;
					power <<= 1;
					continue;
				}
			}
			break;
		}
		reinterpret_cast<LinkedList2Node<UINT8>*>(addr)->key = order;
		emptyAreas[order].addNodeToLast(reinterpret_cast<LinkedList2Node<UINT8>*>(addr));
		
		lock.release();
	}
};

#endif // _BUDDY_H_
