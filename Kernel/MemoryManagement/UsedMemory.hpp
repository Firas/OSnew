
#ifndef	_USED_MEMORY_H_
#define	_USED_MEMORY_H_

#include "MemorySize.hpp"
#include <DataStructure/RedBlackNode.hpp>
#include <DataStructure/SimpleLinkedList/LinkedListNode.hpp>

class UsedMemory{
public:
	unsigned used;
	void *items;
	
	void initialize(){
		used = 0;
		
	}
	
	inline RedBlackNode<MemorySize> *getItem(unsigned index){
		return reinterpret_cast<RedBlackNode<MemorySize>*>(items) + index;
	}
	
}; // class UsedMemory

#endif	// _USED_MEMORY_H_
