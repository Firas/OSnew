
#ifndef	_MEMORY_SIZE_H_
#define	_MEMORY_SIZE_H_

#include <DataStructure/Basic/UnsignedInteger.cpp>

class MemorySize{
public:
	UnsignedInteger address, capacity, usedSize;
	
	CALLCONV MemorySize(UnsignedInteger address, UnsignedInteger usedSize){
		this->address = address;
		this->capacity = this->usedSize = usedSize;
	}
	
	CALLCONV MemorySize(UnsignedInteger address,
			UnsignedInteger usedSize, UnsignedInteger capacity){
		this->address = address;
		this->usedSize = usedSize;
		this->capacity = capacity;
	}
	
	CALLCONV bool operator >(MemorySize &size){
		return address > size.address;
	}
	
	CALLCONV bool operator >=(MemorySize &size){
		return address >= size.address;
	}
	
	CALLCONV bool operator <(MemorySize &size){
		return address < size.address;
	}
	
	CALLCONV bool operator <=(MemorySize &size){
		return address <= size.address;
	}
	
	CALLCONV bool operator ==(MemorySize &size){
		return address == size.address;
	}
	
	CALLCONV bool operator !=(MemorySize &size){
		return address != size.address;
	}
}; // class MemorySize

#endif	// _MEMORY_SIZE_H_
