
#include <iostream>
#include <time.h>
#include "random.cpp"

int main(){
	unsigned i;
	int seed0, seed1;
	__asm__ __volatile__("rdtsc\n\t"
		"movl	%%eax,	%0\n\t"
		"movl	%%eax,	%1" : "=m"(seed0),"=m"(seed1));
	for(i=0; i<100; i++)
		std::cout << rand_vc(&seed0) << '\t';
	std::cout << std::endl << std::endl;
	
	for(i=0; i<100; i++)
		std::cout << rand_posix(&seed1) << '\t';
	std::cout << std::endl << std::endl;
	
	
	seed0 = seed1 = (int)time(0);
	for(i=0; i<100; i++)
		std::cout << rand_vc(&seed0) << '\t';
	std::cout << std::endl << std::endl;
	
	for(i=0; i<100; i++)
		std::cout << rand_posix(&seed1) << '\t';
	std::cout << std::endl << std::endl;
	
	char c;
	unsigned long long ll = seed1;
	ll = (ll << 32) + seed0;
	for(i=0; i<6; i++){
		c = (unsigned)rand_linux(&seed0) % (127 - ' ') + ' ';
		std::cout << c;
		c = (unsigned)rand_pcg32(&ll) % (127 - ' ') + ' ';
		std::cout << c;
	}
	std::cout << std::endl << std::endl;
	
	return 0;
}
