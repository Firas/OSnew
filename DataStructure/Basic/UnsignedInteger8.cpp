
#ifndef _UNSIGNED_INTEGER_8_H_
#define _UNSIGNED_INTEGER_8_H_

#include "Basic.hpp"

class UnsignedInteger8{
protected:
	UINT8 n;
public:
	
	UINT8 MAX_VALUE = ~0;
	
	CALLCONV UnsignedInteger8(UINT8 i=0){
		n = i;
	}
	
	CALLCONV UnsignedInteger8(char *str, UINT8 radix=10){
		n = 0;
		if(str == (char*)0)
		#ifdef _EXCEPTION_H_
			throw NullPointerException(
				"Attempt to construct an UnsignedInteger8 object with NULL string");
		#else
			return;
		#endif
		else if(radix < 2 || radix > 36)
		#ifdef _EXCEPTION_H_
			throw InvalidArgumentException(
				"Attempt to construct an UnsignedInteger8 object with an invalid radix");
		#else
			return;
		#endif
		else{
			char c;
			for(; (c = *str) != 0; str++){
				if(c >= '0' && c <= '9'){
					c -= '0';
				}else if(c >= 'A' && c <= 'Z'){
					c -= 'A' - 10;
				}else if(c >= 'a' && c <= 'z'){
					c -= 'a' - 10;
				}else{
				#ifdef _EXCEPTION_H_
					throw NumberFormatException();
				#endif
					break;
				}
				if(c >= radix){
				#ifdef _EXCEPTION_H_
					throw NumberFormatException();
				#endif
					break;
				}
				n = n * radix + c;
			}
		}
	}
	
	inline int compareTo(UINT8 i){
		return (n > i) ? 1 : (n < i ? -1 : 0);
	}
	
	inline int compareTo(UnsignedInteger8 ui){
		return compareTo(ui.n);
	}
	
	CALLCONV char* toString(char* buffer, UINT8 radix){
		char temp[8 * sizeof(UINT8)];
		if(this == (UnsignedInteger8*)0)
		#ifdef _EXCEPTION_H_
			throw NullPointerException(
				"Attempt to convert a NULL UnsignedInteger8 to a string");
		#else
			return (char*)0;
		#endif
		if(buffer == (char*)0)
		#ifdef _EXCEPTION_H_
			throw NullPointerException(
				"Attempt to convert a UnsignedInteger8 to a NULL string");
		#else
			return (char*)0;
		#endif
		if(radix < 2 || radix > 36)
		#ifdef _EXCEPTION_H_
			throw InvalidArgumentException(
				"Attempt to convert an UnsignedInteger8 object to a string with an invalid radix");
		#else
			return (char*)0;
		#endif
		
		if(n == 0){
			buffer[0] = '0'; buffer[1] = 0;
			return buffer;
		}
		int i = 0, j = 0;
		unsigned q = n, r;
		while(q != 0){
			r = q % radix;
			q = q / radix;
			temp[i] = r + '0';
			if(temp[i] > '9') temp[i] += 'A'-'9'-1;
			i++;
		}
		do{
			buffer[j++] = temp[i--]
		}while(i > 0);
		return buffer;
	}
	
}; // class UnsignedInteger8
#endif // _UNSIGNED_INTEGER_8_H_
