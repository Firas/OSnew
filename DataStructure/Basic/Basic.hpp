
#ifndef	_BASIC_TYPE_H_
#define	_BASIC_TYPE_H_

typedef signed char	INT8;
typedef unsigned char	UINT8;
typedef signed short	INT16;
typedef unsigned short	UINT16;
typedef signed int	INT32;
typedef unsigned int	UINT32;
typedef signed long long	INT64;
typedef unsigned long long	UINT64;

typedef union{
	UINT16 value;
	struct{
		UINT8 byte0;
		UINT8 byte1;
	}bytes;
}UINT16_U;

typedef union{
	UINT32 value;
	struct{
		UINT16 word0;
		UINT16 word1;
	}words;
	struct{
		UINT8 byte0;
		UINT8 byte1;
		UINT8 byte2;
		UINT8 byte3;
	}bytes;
}UINT32_U;

typedef union{
	UINT64 value;
	struct{
		UINT32 dword0;
		UINT32 dword1;
	}dwords;
	struct{
		UINT16 word0;
		UINT16 word1;
		UINT16 word2;
		UINT16 word3;
	}words;
	struct{
		UINT8 byte0;
		UINT8 byte1;
		UINT8 byte2;
		UINT8 byte3;
		UINT8 byte4;
		UINT8 byte5;
		UINT8 byte6;
		UINT8 byte7;
	}bytes;
}UINT64_U;

#define CALLCONV __attribute__((stdcall))

#endif	// _BASIC_TYPE_H_
