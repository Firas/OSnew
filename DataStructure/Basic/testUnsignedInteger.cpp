
#include <iostream>

#include "UnsignedInteger.cpp"

char buffer[64*8+1];

int main(){
	UnsignedInteger ui0;
	std::cout << ui0.toString((char*)buffer) << ' ';
	std::cout << ui0.compareTo(0) << ' ' << ui0.compareTo(1) << std::endl;
	// 0 0 -1
	
	UnsignedInteger ui1(100);
	std::cout << ui1.toString((char*)buffer) << ' ';
	std::cout << ui1.toString((char*)buffer, 16) << ' ';
	std::cout << ui1.toString((char*)buffer, 8) << ' ';
	std::cout << ui1.toString((char*)buffer, 2) << ' ';
	std::cout << ui1.compareTo(10) << ' ' << ui1.compareTo(1000) << std::endl;
	// 100 64 144 1100100 1 -1
	
	UnsignedInteger ui2(UnsignedInteger::MAX_VALUE);
	std::cout << ui2.toString((char*)buffer) << ' ';
	std::cout << ui2.toString((char*)buffer, 16) << ' ';
	std::cout << ui2.compareTo(1000) << ' ' << ui2.compareTo(-1) << std::endl;
	
	return 0;
}
