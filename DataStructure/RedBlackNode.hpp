

#ifndef	_RED_BLACK_NODE_H_
#define	_RED_BLACK_NODE_H_

#include "Basic/Basic.hpp"

template <typename T> class RedBlackTree;
template <typename T> class RedBlackNode{
protected:
	friend class RedBlackTree<T>;
	
	union RedBlackPointer{
		RedBlackNode *node;
		struct{
			unsigned value:1;
		}flag;
		
		CALLCONV RedBlackPointer(RedBlackNode *n = (RedBlackNode*)0) : node(n){
			
		}
	}left, right, parent;
	T key;

public:
	CALLCONV RedBlackNode(T key, RedBlackPointer left = RedBlackPointer(),
			RedBlackPointer right = RedBlackPointer(),
			RedBlackPointer parent = RedBlackPointer()){
		initialize(key, left, right, parent);
	}
	
	inline T getKey(){
		return key;
	}
	
	CALLCONV void initialize(T key, RedBlackPointer left = RedBlackPointer(),
			RedBlackPointer right = RedBlackPointer(),
			RedBlackPointer parent = RedBlackPointer()){
		this->key = key;
		this->left = left; this->right = right;
		this->parent = parent;
	}
	
#ifdef	_TEST_RED_BLACK_
	CALLCONV dump(void printKey(T)){
		RedBlackNode *leftChild = getLeft(), *rightChild = getRight();
		std::cout << this << ": " << std::setw(10) << leftChild << '\t';
		std::cout << std::setw(10) << rightChild << '\t' << std::setw(10) << getParent();
		std::cout << '\t' << isLeft() << '\t' << isRight() << '\t' << isRed() << '\t';
		printKey(key);
		std::cout << std::endl;
		if(leftChild != (RedBlackNode*)0) leftChild->dump(printKey);
		if(rightChild != (RedBlackNode*)0) rightChild->dump(printKey);
	}
#endif
protected:
	inline RedBlackNode* getLeft(){
		RedBlackPointer temp = left;
		temp.flag.value = 0;
		return temp.node;
	}
	inline RedBlackNode* getRight(){
		RedBlackPointer temp = right;
		temp.flag.value = 0;
		return temp.node;
	}
	
	inline RedBlackNode* getParent(){
		RedBlackPointer temp = parent;
		temp.flag.value = 0;
		return temp.node;
	}
	
	inline bool isLeft(){
		return left.flag.value == 1;
	}
	inline bool isRight(){
		return right.flag.value == 1;
	}
	
	inline bool _isRed(){
		return parent.flag.value == 1;
	}
	inline bool _isBlack(){
		return parent.flag.value == 0;
	}
	inline bool isRed(){
		if(this == (RedBlackNode*)0) return false;
		return parent.flag.value == 1;
	}
	inline bool isBlack(){
		if(this == (RedBlackNode*)0) return true;
		return parent.flag.value == 0;
	}
	
	CALLCONV RedBlackNode* getRoot(){
		RedBlackNode *temp = this;
		while(temp->getParent() != (RedBlackNode*)0){
			temp = temp->getParent();
		}
		return temp;
	}
	
public:
	CALLCONV RedBlackNode* getPreviousNode(){
		if(this == (RedBlackNode*)0) return (RedBlackNode*)0;
		RedBlackNode *temp = getLeft(), *result = (RedBlackNode*)0;
		if(temp != (RedBlackNode*)0){
			do{
				result = temp;
				temp = temp->getRight();
			}while(temp != (RedBlackNode)0);
			return result;
		}
		temp = this;
		while(temp->isLeft()) temp = temp->getParent();
		if(temp->isRight()) return temp->getParent();
		return (RedBlackNode*)0;
	}
	CALLCONV RedBlackNode* getNextNode(){
		if(this == (RedBlackNode*)0) return (RedBlackNode*)0;
		RedBlackNode *temp = getRight(), *result = (RedBlackNode*)0;
		if(temp != (RedBlackNode*)0){
			do{
				result = temp;
				temp = temp->getLeft();
			}while(temp != (RedBlackNode)0);
			return result;
		}
		temp = this;
		while(temp->isRight()) temp = temp->getParent();
		if(temp->isLeft()) return temp->getParent();
		return (RedBlackNode*)0;
	}
	
protected:
	CALLCONV void setLeftChild(RedBlackNode *node){
		RedBlackPointer temp(node);
		temp.flag.value = left.flag.value;
		left = temp;
		if(node == (RedBlackNode*)0) return;
		node->setParent(this);
		node->setLeftFlag();
	}
	CALLCONV void setRightChild(RedBlackNode *node){
		RedBlackPointer temp(node);
		temp.flag.value = right.flag.value;
		right = temp;
		if(node == (RedBlackNode*)0) return;
		node->setParent(this);
		node->setRightFlag();
	}
	CALLCONV void _setLeftChild(RedBlackNode *node){
		RedBlackPointer temp(node);
		temp.flag.value = left.flag.value;
		left = temp;
		node->setParent(this);
		node->setLeftFlag();
	}
	CALLCONV void _setRightChild(RedBlackNode *node){
		RedBlackPointer temp(node);
		temp.flag.value = right.flag.value;
		right = temp;
		node->setParent(this);
		node->setRightFlag();
	}
	CALLCONV void _addLeftLeaf(RedBlackNode *node){
		RedBlackPointer temp(node);
		temp.flag.value = left.flag.value;
		left = temp;
		
		temp.node = this;
		temp.flag.value = 1;
		node->parent = temp;
		node->setLeftFlag();
	}
	CALLCONV void _addRightLeaf(RedBlackNode *node){
		RedBlackPointer temp(node);
		temp.flag.value = right.flag.value;
		right = temp;
		
		temp.node = this;
		temp.flag.value = 1;
		node->parent = temp;
		node->setRightFlag();
	}
	CALLCONV void setParent(RedBlackNode *node){
		RedBlackPointer temp(node);
		temp.flag.value = parent.flag.value;
		parent = temp;
	}
	
	inline void setLeftFlag(){
		left.flag.value = 1;
		right.flag.value = 0;
	}
	inline void setRightFlag(){
		left.flag.value = 0;
		right.flag.value = 1;
	}
	
	inline void setRed(){
		parent.flag.value = 1;
	}
	inline void setBlack(){
		parent.flag.value = 0;
	}
	
	CALLCONV unsigned _getSize(){
		unsigned size = 1;
		RedBlackNode *temp = getLeft();
		if(temp != (RedBlackNode*)0) size += temp->_getSize();
		temp = getRight();
		if(temp != (RedBlackNode*)0) size += temp->_getSize();
		return size;
	}
	CALLCONV unsigned getSize(){
		if(this == (RedBlackNode*)0) return 0;
		
		unsigned size = 1;
		RedBlackNode *temp = getLeft();
		if(temp != (RedBlackNode*)0) size += temp->_getSize();
		temp = getRight();
		if(temp != (RedBlackNode*)0) size += temp->_getSize();
		return size;
	}
	
	/*
	 * 要改变的域：
	 * 原父的左/右子		<=	原右子（x）
	 * 原右子的父			<=	原父
	 * 原右子的左标志		<=	原左标志
	 * 原右子的右标志		<=	原右标志
	 * 右子				<=	原右子的原左子（y）
	 * 原右子的原左子的父		<=	本节点
	 * 原右子的原左子的左标志	<=	0
	 * 原右子的原左子的右标志	<=	1
	 * 原右子的左子			<=	本节点
	 * 父				<=	原右子
	 * 左标志			<=	1
	 * 右标志			<=	0
	 * 红标志			<=	原右子的原红标志
	 * 原右子的红标志		<=	本节点的原红标志
	 */
	CALLCONV void rotateLeft(){
		RedBlackNode *x = getRight(), *y = x->getLeft();
		if(isLeft()) getParent()->_setLeftChild(x);
		else if(isRight()) getParent()->_setRightChild(x);
		else{
			x->left.flag.value = 0;
			x->right.flag.value = 0;
		}
		bool isXRed = x->_isRed();
		x->parent = parent;
		
		setRightChild(y);
		x->_setLeftChild(this);
		parent.flag.value = isXRed ? 1 : 0;
	}
	CALLCONV void rotateRight(){
		RedBlackNode *x = getLeft(), *y = x->getRight();
		if(isLeft()) getParent()->_setLeftChild(x);
		else if(isRight()) getParent()->_setRightChild(x);
		else{
			x->left.flag.value = 0;
			x->right.flag.value = 0;
		}
		bool isXRed = x->_isRed();
		x->parent = parent;
		
		setLeftChild(y);
		x->_setRightChild(this);
		parent.flag.value = isXRed ? 1 : 0;
	}
}__attribute__((aligned(2)));
// class RedBlackNode<T>

#endif	// _RED_BLACK_NODE_H_
