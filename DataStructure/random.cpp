
#ifndef	_RANDOM_CPP_
#define	_RANDOM_CPP_	1

int rand_vc(int *seed){
	return (*seed = ((*seed * 214013 + 2531011 >> 16) & 0x7FFF));
}

int rand_posix(int *seed){
	return (*seed = ((*seed * 1103515245 + 12345 >> 16) & 0x7FFF));
}

int rand_linux(int *seed){
	return (*seed = *seed * 1664525 + 1013904223);
}

int rand_pcg32(unsigned long long *seed){
	unsigned long long oldState = *seed;
	*seed = oldState * 6364136223846793005ULL + 1;
	int xorShifted = ((oldState >> 18u) ^ oldState) >> 27u;
	int rotateNum = oldState >> 59u;
	return (xorShifted >> rotateNum) | (xorShifted << ((-rotateNum) & 31));
}
#endif	// _RANDOM_CPP_
