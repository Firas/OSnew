#include <iostream>
#include <iomanip>

#define	_TEST_RED_BLACK_
#include "RedBlackTree.hpp"

void printDouble(double d){
	std::cout << d;
}

int main(){
	RedBlackTree<double> tree0;
	RedBlackNode<double> *nodes = (RedBlackNode<double>*)malloc(32 * sizeof(RedBlackNode<double>));
	double key = 0.5;
	for(unsigned i=0; i<=20; i++){
		nodes[i].initialize(key);
		tree0.addNode(nodes+i);
		key += 1.0;
	}
	tree0.dump(printDouble);
	
	//tree0.removeNode(nodes+7);
	tree0._removeNode(nodes+7);
	tree0.dump(printDouble);
	
	//tree0.removeNode(nodes+7);
	tree0._removeNode(nodes+8);
	tree0.dump(printDouble);
	
	//tree0.removeNode(nodes+4);
	tree0._removeNode(nodes+4);
	tree0.dump(printDouble);
	
	return 0;
}
