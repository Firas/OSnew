

#ifndef	_LINKED_LIST_H_
#define	_LINKED_LIST_H_

#include "LinkedListNode.hpp"

template <typename T> class LinkedList{
protected:
	LinkedListNode<T> *head;
public:
	CALLCONV LinkedList(LinkedListNode<T> *firstNode = (LinkedListNode<T>*)0){
		head = firstNode;
	}
	
	CALLCONV LinkedList(T firstElement){
		head = new LinkedListNode<T>(firstElement);
	}
	
	inline bool isEmpty(){
		if(this == (LinkedList*)0) return true;
		if(head == (LinkedListNode<T>*)0) return true;
		return false;
	}
	
	CALLCONV unsigned getSize(){
		if(this == (LinkedList*)0) return 0;
		unsigned size = 0;
		for(LinkedListNode<T> *temp = head;
			temp != (LinkedListNode<T>*)0;
			temp = temp->next) size++;
		return size;
	}
	
	CALLCONV unsigned indexOf(T element){
		if(this == (LinkedList*)0) return ~(unsigned)0;
		unsigned index = 0;
		for(LinkedListNode<T> *temp = head;
				temp != (LinkedListNode<T>*)0;
				temp = temp->next, index++)
			if(temp->key == element) return index;
		return ~(unsigned)0;
	}
	
	CALLCONV bool addElementToFirst(T element){
		if(this == (LinkedList*)0) return false;
		head = new LinkedListNode<T>(element, head);
		return true;
	}
	
	CALLCONV bool addElementToLast(T element){
		if(this == (LinkedList*)0) return false;
#if 0	// method 1
		if(head == (LinkedListNode<T>*)0){
			head = new LinkedListNode<T>(element);
		}else{
			LinkedListNode<T> *temp = head;
			while(temp->next != (LinkedListNode<T>*)0)
				temp = temp->next;
			temp->next = new LinkedListNode<T>(element);
		}
#else
		LinkedListNode<T> **temp = &head;
		while(*temp != (LinkedListNode<T>*)0)
			temp = &((*temp)->next);
		*temp = new LinkedListNode<T>(element);
#endif
		return true;
	}
	
	CALLCONV bool setFirstElement(T element){
		if(this == (LinkedList*)0) return false;
		if(head == (LinkedListNode<T>*)0) return false;
		head->key = element;
		return true;
	}
	
	CALLCONV bool setLastElement(T element){
		if(this == (LinkedList*)0) return false;
		if(head == (LinkedListNode<T>*)0) return false;
		LinkedListNode<T> *temp = head;
		while(temp->next != (LinkedListNode<T>*)0)
			temp = temp->next;
		temp->key = element;
		return true;
	}
	
	CALLCONV bool getFirstElement(T *pe){
		if(this == (LinkedList*)0) return false;
		if(head == (LinkedListNode<T>*)0) return false;
		if(pe == (T*)0) return false;
		*pe = head->key;
		return true;
	}
	
	CALLCONV bool getLastElement(T *pe){
		if(this == (LinkedList*)0) return false;
		if(head == (LinkedListNode<T>*)0) return false;
		if(pe == (T*)0) return false;
		LinkedListNode<T> *temp = head;
		while(temp->next != (LinkedListNode<T>*)0)
			temp = temp->next;
		*pe = temp->key;
		return true;
	}
	
	CALLCONV bool removeFirstElement(T *pe){
		if(this == (LinkedList*)0) return false;
		if(head == (LinkedListNode<T>*)0) return false;
		if(pe != (T*)0) *pe = head->key;
		LinkedListNode<T> *temp = head;
		head = head->next;
		delete temp;
		return true;
	}
	
	CALLCONV bool removeLastElement(T *pe){
		if(this == (LinkedList*)0) return false;
		if(head == (LinkedListNode<T>*)0) return false;
#if 0	// method 1
		LinkedListNode<T> *temp0 = (LinkedListNode<T>*)0, *temp1 = head;
		while(temp1->next != (LinkedListNode<T>*)0){
			temp0 = temp1;
			temp1 = temp1->next;
		}
		if(pe != (T*)0) *pe = temp1->key;
		if(head == temp1){
			head = (LinkedListNode<T>*)0;
			delete temp1;
		}else{
			temp0 = (LinkedListNode<T>*)0;
			delete temp1;
		}
#else
		LinkedListNode<T> **temp = &head;
		while((*temp)->next != (LinkedListNode<T>*)0)
			temp = &((*temp)->next);
		if(pe != (T*)0) *pe = (*temp)->key;
		delete *temp;
		*temp = (LinkedListNode<T>*)0;
#endif
		return true;
	}
	
	
}; // class LinkedList<T>

#endif	// _LINKED_LIST_H_
