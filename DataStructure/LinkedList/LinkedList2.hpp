

#ifndef	_LINKED_LIST_2_H_
#define	_LINKED_LIST_2_H_

#include "LinkedList2Node.hpp"

template <typename T> class LinkedList2{
protected:
	LinkedList2Node<T> *head;
public:
	CALLCONV LinkedList2(LinkedList2Node<T> *firstNode = (LinkedList2Node<T>*)0){
		head = firstNode;
	}
	
	CALLCONV LinkedList2(T firstElement){
		head = new LinkedList2Node<T>(firstElement);
	}
	
	inline bool isEmpty(){
		if(this == (LinkedList2*)0) return true;
		if(head == (LinkedList2Node<T>*)0) return true;
		return false;
	}
	
	CALLCONV unsigned getSize(){
		if(this == (LinkedList2*)0) return 0;
		unsigned size = 0;
		for(LinkedList2Node<T> *temp = head;
			temp != (LinkedList2Node<T>*)0;
			temp = temp->next) size++;
		return size;
	}
	
	CALLCONV unsigned indexOf(T element){
		if(this == (LinkedList2*)0) return ~(unsigned)0;
		unsigned index = 0;
		for(LinkedList2Node<T> *temp = head;
				temp != (LinkedList2Node<T>*)0;
				temp = temp->next, index++)
			if(temp->key == element) return index;
		return ~(unsigned)0;
	}
	
	CALLCONV bool addElementToFirst(T element){
		if(this == (LinkedList2*)0) return false;
		head = new LinkedList2Node<T>(element, head);
		if(head->next != (LinkedList2Node<T>*)0) head->next->prev = head;
		return true;
	}
	
	CALLCONV bool addElementToLast(T element){
		if(this == (LinkedList2*)0) return false;
		if(head == (LinkedList2Node<T>*)0){
			head = new LinkedList2Node<T>(element);
		}else{
			LinkedList2Node<T> *temp = head;
			while(temp->next != (LinkedList2Node<T>*)0)
				temp = temp->next;
			temp->next = new LinkedList2Node<T>(element,
				(LinkedList2Node<T>*)0, temp);
		}
		return true;
	}
	
	CALLCONV bool setFirstElement(T element){
		if(this == (LinkedList2*)0) return false;
		if(head == (LinkedList2Node<T>*)0) return false;
		head->key = element;
		return true;
	}
	
	CALLCONV bool setLastElement(T element){
		if(this == (LinkedList2*)0) return false;
		if(head == (LinkedList2Node<T>*)0) return false;
		LinkedList2Node<T> *temp = head;
		while(temp->next != (LinkedList2Node<T>*)0)
			temp = temp->next;
		temp->key = element;
		return true;
	}
	
	CALLCONV bool getFirstElement(T *pe){
		if(this == (LinkedList2*)0) return false;
		if(head == (LinkedList2Node<T>*)0) return false;
		if(pe == (T*)0) return false;
		*pe = head->key;
		return true;
	}
	
	CALLCONV bool getLastElement(T *pe){
		if(this == (LinkedList2*)0) return false;
		if(head == (LinkedList2Node<T>*)0) return false;
		if(pe == (T*)0) return false;
		LinkedList2Node<T> *temp = head;
		while(temp->next != (LinkedList2Node<T>*)0)
			temp = temp->next;
		*pe = temp->key;
		return true;
	}
	
	CALLCONV bool removeFirstElement(T *pe){
		if(this == (LinkedList2*)0) return false;
		if(head == (LinkedList2Node<T>*)0) return false;
		if(pe != (T*)0) *pe = head->key;
		LinkedList2Node<T> *temp = head;
		if((head = head->next) != (LinkedList2Node<T>*)0)
			head->prev = (LinkedList2Node<T>*)0;
		delete temp;
		return true;
	}
	
	CALLCONV bool removeLastElement(T *pe){
		if(this == (LinkedList2*)0) return false;
		if(head == (LinkedList2Node<T>*)0) return false;
#if 0	// method 1
		LinkedList2Node<T> *temp = head;
		while(temp->next != (LinkedList2Node<T>*)0){
			temp = temp->next;
		}
		if(pe != (T*)0) *pe = temp->key;
		if(head == temp){
			head = (LinkedList2Node<T>*)0;
			delete temp;
		}else{
			temp->prev->next = (LinkedList2Node<T>*)0;
			delete temp;
		}
#else
		LinkedList2Node<T> **temp = &head;
		while((*temp)->next != (LinkedList2Node<T>*)0)
			temp = &((*temp)->next);
		if(pe != (T*)0) *pe = (*temp)->key;
		delete *temp;
		*temp = (LinkedList2Node<T>*)0;
#endif
		return true;
	}
	
	
}; // class LinkedList2<T>

#endif	// _LINKED_LIST_2_H_
