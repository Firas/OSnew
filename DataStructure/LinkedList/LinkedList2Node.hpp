

#ifndef	_LINKED_LIST_2_NODE_H_
#define	_LINKED_LIST_2_NODE_H_

#include "../Basic/Basic.hpp"

template <typename T> class LinkedList2;
template <typename T> class LinkedListO2;
template <typename T> class LinkedQueue2;

template <typename T> class LinkedList2Node{
protected:
	LinkedList2Node *next, *prev;
	T key;
	
	friend class LinkedList2<T>;
	friend class LinkedListO2<T>;
	friend class LinkedQueue2<T>;
public:
	CALLCONV LinkedList2Node(T element,
			LinkedList2Node *nextNode = (LinkedList2Node*)0,
			LinkedList2Node *prevNode = (LinkedList2Node*)0){
		key = element;
		next = nextNode;
		prev = prevNode;
	}
	
	inline T getKey(){
		return key;
	}
}; // class LinkedList2Node<T>

#endif	// _LINKED_LIST_NODE_2_H_
