

#include <iostream>

#include "LinkedQueue2.hpp"

int main(){
	LinkedQueue2<double> *list0 = (LinkedQueue2<double>*)0;
	double element = 0.0;
	std::cout << std::boolalpha << list0->isEmpty() << ' ';
	std::cout << list0->getSize() << ' ';
	std::cout << std::hex << list0->indexOf(0.0) << std::endl;
	// true 0 ffffffff
	std::cout << std::boolalpha << list0->addElementToFirst(0.5) << ' ';
	std::cout << std::boolalpha << list0->addElementToLast(1.5) << ' ';
	std::cout << std::boolalpha << list0->setFirstElement(2.5) << ' ';
	std::cout << std::boolalpha << list0->setLastElement(3.5) << std::endl;
	// false false false false
	std::cout << std::boolalpha << list0->getFirstElement(&element) << ' ';
	std::cout << std::boolalpha << list0->getLastElement(&element) << ' ';
	std::cout << element << ' ';
	std::cout << std::boolalpha << list0->removeFirstElement(&element) << ' ';
	std::cout << std::boolalpha << list0->removeLastElement(&element) << std::endl;
	// false false 0.0 false false
	
	std::cout << std::endl;
	list0 = new LinkedQueue2<double>();
	std::cout << std::boolalpha << list0->isEmpty() << ' ';
	std::cout << list0->getSize() << ' ';
	std::cout << std::hex << list0->indexOf(0.0) << '\t';
	std::cout << std::boolalpha << list0->setFirstElement(2.5) << ' ';
	std::cout << std::boolalpha << list0->setLastElement(3.5) << ' ';
	std::cout << std::boolalpha << list0->getFirstElement(&element) << ' ';
	std::cout << std::boolalpha << list0->getLastElement(&element) << std::endl;
	// true 0 ffffffff	false false false false
	std::cout << std::boolalpha << list0->addElementToLast(1.5) << ' ';
	std::cout << std::boolalpha << list0->addElementToFirst(0.5) << '\t';
	std::cout << std::boolalpha << list0->isEmpty() << ' ';
	std::cout << list0->getSize() << '\t';
	std::cout << std::hex << list0->indexOf(0.0) << ' ';
	std::cout << list0->indexOf(0.5) << ' ' << list0->indexOf(1.5) << std::endl;
	// true true	false 2	ffffffff 0 1
	std::cout << std::boolalpha << list0->setFirstElement(2.5) << ' ';
	std::cout << std::boolalpha << list0->setLastElement(3.5) << ' ';
	std::cout << std::hex << list0->indexOf(0.5) << std::endl;
	// true true ffffffff
	std::cout << std::boolalpha << list0->getFirstElement(&element) << ' ';
	std::cout << element << ' ';
	std::cout << std::boolalpha << list0->getLastElement(&element) << ' ';
	std::cout << element << ' ';
	std::cout << std::boolalpha << list0->removeLastElement(&element) << ' ';
	std::cout << element << ' ';
	std::cout << std::boolalpha << list0->removeFirstElement(&element) << ' ';
	std::cout << element << std::endl;
	// true 2.5 true 3.5 true 3.5 true 2.5
	std::cout << std::boolalpha << list0->isEmpty() << ' ';
	std::cout << list0->getSize() << ' ';
	std::cout << std::hex << list0->indexOf(3.5) << std::endl;
	// true 0 ffffffff
	std::cout << std::boolalpha << list0->addElementToFirst(0.5) << ' ';
	std::cout << std::boolalpha << list0->addElementToLast(1.5) << ' ';
	std::cout << std::boolalpha << list0->addElementToLast(2.5) << '\t';
	std::cout << std::boolalpha << list0->isEmpty() << ' ';
	std::cout << list0->getSize() << ' ';
	std::cout << list0->indexOf(2.5) << std::endl;
	// true true true  false 3 2
	
	return 0;
}
