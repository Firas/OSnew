

#ifndef	_LINKED_LIST_NODE_H_
#define	_LINKED_LIST_NODE_H_

#include "../Basic/Basic.hpp"

template <typename T> class LinkedList;
template <typename T> class LinkedListO;
template <typename T> class LinkedQueue;

template <typename T> class LinkedListNode{
protected:
	LinkedListNode *next;
	T key;
	
	friend class LinkedList<T>;
	friend class LinkedListO<T>;
	friend class LinkedQueue<T>;
public:
	CALLCONV LinkedListNode(T element, LinkedListNode *nextNode = (LinkedListNode*)0){
		key = element;
		next = nextNode;
	}
	
	inline T getKey(){
		return key;
	}
}; // class LinkedListNode<T>

#endif	// _LINKED_LIST_NODE_H_
