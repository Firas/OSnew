

#ifndef	_LINKED_LIST_2_NODE_H_
#define	_LINKED_LIST_2_NODE_H_

#include "../Basic/Basic.hpp"

template <typename T> class LinkedList2Node{
public:
	LinkedList2Node *next, *prev;
	T key;
	
	CALLCONV LinkedList2Node(T element,
			LinkedList2Node *nextNode = (LinkedList2Node*)0,
			LinkedList2Node *prevNode = (LinkedList2Node*)0){
		key = element;
		next = nextNode;
		prev = prevNode;
	}
	
	CALLCONV bool initialize(T element,
			LinkedList2Node *nextNode = (LinkedList2Node*)0,
			LinkedList2Node *prevNode = (LinkedList2Node*)0){
		if(this == (LinkedList2Node*)0) return false;
		key = element;
		next = nextNode;
		prev = prevNode;
		return true;
	}
	
}; // class LinkedList2Node<T>

#endif	// _LINKED_LIST_NODE_2_H_
