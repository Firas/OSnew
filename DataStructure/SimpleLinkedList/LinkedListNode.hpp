

#ifndef	_LINKED_LIST_NODE_H_
#define	_LINKED_LIST_NODE_H_

#include "../Basic/Basic.hpp"

template <typename T> class LinkedListNode{
public:
	LinkedListNode *next;
	T key;
	
	CALLCONV LinkedListNode(T element, LinkedListNode *nextNode = (LinkedListNode*)0){
		key = element;
		next = nextNode;
	}
	
	CALLCONV bool initialize(T element, LinkedListNode *nextNode = (LinkedListNode*)0){
		if(this == (LinkedListNode*)0) return false;
		key = element;
		next = nextNode;
		return true;
	}
}; // class LinkedListNode<T>

#endif	// _LINKED_LIST_NODE_H_
