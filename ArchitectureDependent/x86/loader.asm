FORMAT MS COFF

INCLUDE "../LegacyBIOS/loader.asm"
INCLUDE "cpuid.asm"
INCLUDE "Page32.asm"

DISPLAY_OFFSET	readDiskInReal
	push	DWORD MEM_MAP_ADDR
	call	watchMemory
	
	call	disableProtectedMode
USE16	
	mov	ax,	41h
	mov	dl,	[BootDriveNum]
	int	13h
	
	call	far 0:enableProtectedMode32
USE32
	call	enablePage32
	movzx	eax,	WORD [40Eh]
	movzx	ecx,	WORD [413h]
	call	watchGeneralRegisters
	
	push	DWORD 20000h
	call	getPageTableEntry
	jmp	die
	
INCLUDE "debug32.asm"

