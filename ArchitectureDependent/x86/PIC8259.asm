; 可编程中断控制器8059

; Programmable Interrupt Controller 8259

PIC1_COMMAND	EQU	020h
PIC1_DATA	EQU	021h
PIC2_COMMAND	EQU	0A0h
PIC2_DATA	EQU	0A1h

ICW1_ICW4	EQU	1	; ICW4 (not) needed
ICW1_SINGLE	EQU	2	; Single (cascade) mode
ICW1_INTERVAL4	EQU	4	; Call address interval 4 (8)
ICW1_LEVEL	EQU	8	; Level triggered (edge) mode
ICW1_INIT	EQU	10h	; Initialization - required!

ICW4_8086	EQU	1	; 8086/88 (MCS-80/85) mode
ICW4_AUTO	EQU	2	; Auto EOI
ICW4_BUF_SLAVE	EQU	8	; Buffered mode/slave
ICW4_BUF_MASTER	EQU	0Ch	; Buffered mode/master
ICW4_SFNM	EQU	10h	; Special fully nested (not)

PIC_EOI		EQU	20h	; non-specific End Of Interrupt Command

remapPIC8259:
; 开始重新初始化PIC
	mov	al,	ICW1_INIT + ICW1_ICW4
	out	PIC1_COMMAND,	al
	out	PIC2_COMMAND,	al

; 重映射PIC1中断到30h-37h，重映射PIC2中断到38h-3Fh
	mov	al,	30h
	out	PIC1_DATA,	al
	mov	al,	38h
	out	PIC2_DATA,	al

; 主PIC的IRQ2连接到从PIC
	mov	al,	04h
	out	PIC1_DATA,	al
	; ICW3: tell Master PIC that there is a slave PIC at IRQ2 (0000 0100)
	mov	al,	02h
	out	PIC2_DATA,	al
	; ICW3: tell Slave PIC its cascade identity (0000 0010)

	mov	al,	ICW4_8086
	out	PIC1_DATA,	al
	out	PIC2_DATA,	al
	ret

enablePIC8259:
	xor	al,	al
	out	PIC1_DATA,	al
	out	PIC2_DATA,	al
	ret

disablePIC8259:
	mov	al,	0FFh
	out	PIC2_DATA,	al	; mask all interrupt on the secondary PIC
	mov	al,	0FBh	; comment this instruction if mask all interrupt on the primary PIC
	out	PIC1_DATA,	al	; mask all but cascade on the primary PIC
	ret
