; ���ƼĴ���
; Control Registers

; CR0
CR0.PE    EQU         1h	; Protection Enable
CR0.MP    EQU         2h	; Monitor Coprocessor for FPU
CR0.EM    EQU         4h	; Emulation for FPU
CR0.TS    EQU         8h	; Task Switch Flag for FPU
CR0.ET    EQU        10h	; Extension Type for FPU
CR0.NE    EQU        20h	; Numeric Error for FPU
CR0.WP    EQU     10000h	; Write Protect
CR0.AM    EQU     40000h	; Alignment Mask
CR0.NW    EQU  20000000h	; Not Write Through
CR0.CD    EQU  40000000h	; Cache Disable
CR0.PG    EQU  80000000h	; Paging

; CR1 is reserved
; CR2 is for Page-Fault Linear Address

; CR3 (PDBR)
CR3.PWT     EQU                   8h	; Write Through
CR3.PCD     EQU                  10h	; Cache Disable
CR3.PDBASE  EQU           0FFFFF000h
CR3.PML4BASE  EQU  7FFFFFFFFFFFF000h	; For PAE and IA-32e Paging

; CR4
CR4.VME         EQU          1h	; Virtual-8086 Mode Extension
CR4.PVI         EQU          2h	; Protected-Mode Virtual Interrupt
CR4.TSD         EQU          4h	; Time Stamp Disable
CR4.DE          EQU          8h	; Debugging Extensions
CR4.PSE         EQU         10h	; Page Size Extensions
CR4.PAE         EQU         20h	; Physical Address Extension
CR4.MCE         EQU         40h	; Machine-Check Enable
CR4.PGE         EQU         80h	; Page Global Enable
CR4.PCE         EQU        100h	; Performance-Monitoring Counter Enable
CR4.OSFXSR      EQU        200h	; Operating System Support for FXSAVE and FXRSTOR instructions
CR4.OSXMMEXCPT  EQU        400h	; Operating System Support for Unmasked SIMD Floating-Point Exceptions
CR4.VMXE        EQU       2000h	; VMX-Enable
CR4.SMXE        EQU       4000h	; SMX-Enable
CR4.FSGSBASE    EQU      10000h	; FSGSBASE-Enable
CR4.PCIDE       EQU      20000h	; PCID-Enable
CR4.OSXSAVE     EQU      40000h	; XSAVE and Processor Extended States-Enable
CR4.SMEP        EQU     100000h	; SMEP-Enable

; CR8
CR8.TPL		EQU	0Fh	; Task Priority Level
