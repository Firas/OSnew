; 32位分页数据结构
; 32-bit Paging Structure

IF ~DEFINED _PAGE_32_H_
_PAGE_32_H_	EQU	1

; 页目录项
; Page Directory Entry (PDE)
PDE.PRESENT	EQU	1
PDE.R_W		EQU	2	; Writeable
PDE.U_S		EQU	4	; accessable to application (User level)
PDE.PWT		EQU	8	; Write-Through
PDE.PCD		EQU	10h	; Cache Disable
PDE.ACCESSED	EQU	20h
PDE.DIRTY	EQU	40h
PDE.PAGE_SIZE	EQU	80h	; 4MB Page Size
PDE.GLOBAL	EQU	100h
PDE.PAT		EQU	1000h	; Page Attribute Table
PDE.HIGH_ADDR	EQU	0E000h
PDE.LOW_ADDR	EQU	0FFC00000h
PDE.PT_ADDR	EQU	0FFFFF000h	; Page Table Address

; 页表项
; Page Table Entry (PTE)
PTE.PRESENT     EQU	1
PTE.R_W		EQU	2
PTE.U_S		EQU	4
PTE.PWT		EQU	8
PTE.PCD		EQU	10h
PTE.ACCESSED	EQU	20h
PTE.DIRTY	EQU	40h
PTE.PAT		EQU	80h
PTE.GLOBAL	EQU	100h
PTE.ADDR	EQU	0FFFFF000h

; 页面错误错误码
; Page Error Code
PAGEFAULT.PRESENT	EQU	1
PAGEFAULT.W_R		EQU	2
PAGEFAULT.U_S		EQU	4
PAGEFAULT.RSVB		EQU	8
PAGEFAULT.I_D		EQU	10h

PAGE_SIZE0	EQU	1000h
PAGE_SIZE1	EQU	400000h

DISPLAY_OFFSET	initPage32
emptyPagingStructures:
	cld
	mov	edi,	PAGE_STRUC1
	mov	ecx,	2*1024
	xor	eax,	eax
	rep	stosd

setFirstPageDirEntry:
	mov	edi,	PAGE_STRUC1
	mov	DWORD [edi],	PAGE_STRUC2+PDE.PRESENT+PDE.R_W
	mov	cr3,	edi

setFirstPageTableEntries:
	mov	edi,	PAGE_STRUC2
	mov	esi,	0A0000h
	mov	eax,	PTE.PRESENT+PTE.R_W
.loop0:
	stosd
	add	eax,	4*1024
	cmp	eax,	esi
	jbe	.loop0
	
	mov	edi,	PAGE_STRUC2+0A0000h/1024	; 0A0000h/1000h*4
	mov	esi,	100000h
	mov	eax,	0A0000h+PTE.PRESENT+PTE.R_W+PTE.PWT+PTE.PCD
.loop1:
	stosd
	add	eax,	4*1024
	cmp	eax,	esi
	jbe	.loop1
	
	call	enablePage32
	sti
	jmp	endInitPage32
	
enablePage32:
	mov	eax,	cr0
	or	eax,	CR0.PG
	mov	cr0,	eax
	ret
	
disablePage32:
	mov	eax,	cr0
	and	eax,	NOT CR0.PG
	mov	cr0,	eax
	ret
	
; PDE* getPageDirEntry(void *address);
public getPageDirEntry as '_getPageDirEntry@4'
getPageDirEntry:
	mov	eax,	cr3
	and	eax,	0FFFFF000h
	
	mov	ecx,	[esp+4]
	shr	ecx,	22
	lea	eax,	[eax+4*ecx]
	retn	4
	
; PTE* getPageTableEntry(void *address);
public getPageTableEntry as '_getPageTableEntry@4'
getPageTableEntry:
	mov	eax,	cr3
	and	eax,	0FFFFF000h
	
	mov	ecx,	[esp+4]
	shr	ecx,	22
	lea	eax,	[eax+4*ecx]
	; 先右移22位再乘以4，这样不需要将低2位清0
	
	test	BYTE [eax],	PDE.PRESENT
	jnz	@F
	
	; 内存区域的存在位为0
	or	al,	1
	; 标记此项为页目录表中的对应4MB内存区域的项
	retn	4
@@:	test	BYTE [eax],	PDE.PAGE_SIZE
	jz	@F
	
	; 4MB大页面
	or	al,	1
	; 标记此项为页目录表中的对应4MB内存区域的项
	retn	4
	
@@:	mov	eax,	[eax]
	and	eax,	0FFFFF000h
	
	mov	ecx,	[esp+4]
	shr	ecx,	12
	and	ecx,	3FFh
	lea	eax,	[eax+4*ecx]
	retn	4

endInitPage32:

END IF ; _PAGE_32_H_
