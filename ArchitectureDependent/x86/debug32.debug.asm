
ALIGN 2
DebugRegStr:	db	'   eax: 00000000', 0
DebugTab:	db	9, 0
DebugNewLine:	db	13, 10, 0

; void watchGeneralRegisters();
public watchGeneralRegisters as '_watchGeneralRegisters'
watchGeneralRegisters:
	push	ebp
	mov	ebp,	esp
	pushfd
	push	eax
	push	ecx
	push	edx
	push	ebp	; esp
	
	push	eax
	mov	DWORD [DebugRegStr+2],	' eax'
	call	.middle
	
	push	ebx
	mov	BYTE [DebugRegStr+4],	'b'
	call	.middle
	
	mov	ecx,	[esp+2*4]
	push	ecx
	mov	BYTE [DebugRegStr+4],	'c'
	call	.middle
	
	mov	edx,	[esp+4]
	push	edx
	mov	BYTE [DebugRegStr+4],	'd'
	call	.middle
	call	.endl
	
	push	esi
	mov	WORD [DebugRegStr+4],	'si'
	call	.middle
	
	push	edi
	mov	BYTE [DebugRegStr+4],	'd'
	call	.middle
	
	mov	eax,	[esp+5*4]
	push	eax
	mov	WORD [DebugRegStr+4],	'bp'
	call	.middle
	
	mov	BYTE [DebugRegStr+4],	's'
	add	DWORD [esp],	4
	call	.middle
	call	.endl
	
	pop	edx
	pop	ecx
	pop	eax
	popfd
	pop	ebp
	ret
	
.middle:
	xchg	eax,	[esp]
	xchg	eax,	[esp+4]
	xchg	eax,	[esp]
	push	DebugRegStr+8
	call	UnsignedInteger._toHexString
	push	WORD 0F00h
	push	WORD [TTY.currentRow]
	push	DebugRegStr
	call	TTY.printString
	ret
.endl:
	push	WORD 0F00h
	push	WORD [TTY.currentRow]
	push	DebugTab
	call	TTY.printString
	push	WORD 0F00h
	push	WORD [TTY.currentRow]
	push	DebugNewLine
	call	TTY.printString
	ret
	
DebugMemStr:	db	'00000000: ', 0
DebugHex:	db	'00', 0
DebugChar:	db	' ', 0, 0
public watchMemory as '_watchMemory@4'
watchMemory:
	push	ebp
	mov	ebp,	esp
	pushfd
	push	esi
	push	ebx
	push	eax
	push	ecx
	push	edx
	
	mov	esi,	[ebp+2*4]	; addr
	mov	bl,	16
.loop0:
	push	esi
	push	DebugMemStr
	call	UnsignedInteger._toHexString
	mov	WORD [DebugMemStr+8],	': '
	push	WORD 0F00h
	push	WORD [TTY.currentRow]
	push	DebugMemStr
	call	TTY.printString

	mov	bh,	16
.loop1:
	movzx	eax,	BYTE [esi]
	push	eax
	push	DebugHex
	call	Unsigned8._toHexString
	push	WORD 0F00h
	push	WORD [TTY.currentRow]
	push	DebugHex
	call	TTY.printString
	
	mov	al,	[esi]
	cmp	al,	' '
	jb	.else0
	cmp	al,	127
	jae	.else0
	mov	[DebugChar],	al
	push	WORD 1F00h
	jmp	.endif0
.else0:
	mov	BYTE [DebugChar],	' '
	push	WORD 0F00h
.endif0:
	inc	BYTE [DebugChar+2]
	cmp	BYTE [DebugChar+2],	4
	jae	.else1
	mov	BYTE [DebugChar+1],	0
	jmp	.endif1
.else1:
	mov	WORD [DebugChar+1],	' '
.endif1:
	push	WORD [TTY.currentRow]
	push	DebugChar
	call	TTY.printString
	inc	esi
	dec	bh
	jnz	.loop1

	push	WORD 0F00h
	push	WORD [TTY.currentRow]
	push	DebugTab
	call	TTY.printString
	push	WORD 0F00h
	push	WORD [TTY.currentRow]
	push	DebugNewLine
	call	TTY.printString
	
	dec	bl
	jnz	.loop0
	
	pop	edx
	pop	ecx
	pop	eax
	pop	ebx
	pop	esi
	popfd
	pop	ebp
	retn	4

public watchStack as '_watchStack'
watchStack:
	push	ebp
	mov	ebp,	esp
	push	ebp
	;add	DWORD [esp],	2*4	; ebp and ret
	call	watchMemory
	pop	ebp
	ret

INCLUDE "DataStructure\Basic\Unsigned8.asm"
INCLUDE "DataStructure\Basic\UnsignedInteger.asm"
INCLUDE "tty.asm"
