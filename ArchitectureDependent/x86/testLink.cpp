#include <DataStructure/Basic/UnsignedInteger.cpp>

extern "C" CALLCONV ULONG getPageTableEntry(void *addr);

CALLCONV bool isFree(void *addr){
	ULONG pagingEntryPointer = getPageTableEntry(addr);
	ULONG pagingEntry = *reinterpret_cast<ULONG*>(pagingEntryPointer & ~3);
	return (pagingEntry & 0x801) == 1;
}
