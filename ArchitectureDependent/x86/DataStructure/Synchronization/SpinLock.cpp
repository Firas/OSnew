
#ifndef	_SPIN_LOCK_CPP_
#define	_SPIN_LOCK_CPP_

#include <DataStructure/Basic/Basic.hpp>

class SpinLock{
private:
	volatile UINT8 value;
public:
	SpinLock(){
		value = 0;
	}
	
	void get(){
		__asm__ __volatile__(
			".L1:"
			"xchgb	%b1,	%0\n\t"
			"testb	%b1,	%b1\n\t"
			"jnz	.L1\n\t" : "=m"(value) : "a"(1));
	}
	
	void release(){
		value = 0;
	}
}; // class SpinLock

#endif	// _SPIN_LOCK_CPP_
