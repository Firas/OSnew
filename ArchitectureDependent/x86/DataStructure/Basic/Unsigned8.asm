

IF ~DEFINED _UNSIGNED_8_H_
_UNSIGNED_8_H_	EQU	1


; void Unsigned8::_toHexString(char* buffer, unsigned i);
Unsigned8._toHexString:
	mov	edx,	[esp+4]	; buffer
	test	edx,	edx
	jz	.return
	mov	BYTE [edx+2],	0
	mov	al,	[esp+2*4]
	mov	ah,	al
	shr	al,	4
	and	ah,	0Fh
	or	ax,	'00'
	cmp	al,	'9'
	jna	@F
	add	al,	'A'-'9'-1
@@:	cmp	ah,	'9'
	jna	@F
	add	ah,	'A'-'9'-1
@@:	mov	[edx],	ax
.return:
	retn	2*4
	
END IF ; _UNSIGNED_8_H_
