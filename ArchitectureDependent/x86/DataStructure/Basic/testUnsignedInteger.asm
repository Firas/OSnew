
format PE console
entry start
include "C:\FASM\INCLUDE\win32a.inc"

section '.code' code readable executable
start:
	;int3
	
	push	DWORD 0123CDEFh
	push	buffer
	call	UnsignedInteger._toHexString
	
	push	buffer
	call	[puts]
	
	mov	DWORD [esp],	0
	call	[ExitProcess]
	
INCLUDE "UnsignedInteger.asm"

section '.data' data readable writeable

result	dd 0
buffer	db 68 dup(0)

section '.idata' import data readable
library	kernel, 'kernel32.dll',\
	user, 'user32.dll',\
	msvcrt, 'msvcrt.dll'
import	kernel, \
	ExitProcess, 'ExitProcess'
import	user,\
	MessageBoxA, 'MessageBoxA',\
	MessageBoxW, 'MessageBoxW'
import	msvcrt,\
	printf,'printf',\
	puts,'puts',\
	getchar,'getchar'
