

#ifndef	_X86_IO_H_
#define	_X86_IO_H_

#include "../../../DataStructure/Basic/Basic.hpp"

class X86IO{
public:
	static inline void outputByte(UINT16 port, UINT8 data);
	static inline UINT8 inputByte(UINT16 port);
	static inline void outputWord(UINT16 port, UINT16 data);
	static inline UINT16 inputWord(UINT16 port);
	static inline void outputDword(UINT16 port, UINT32 data);
	static inline UINT32 inputDword(UINT16 port);
};

#endif // _X86_IO_H_
