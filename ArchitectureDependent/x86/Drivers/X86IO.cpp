

#ifndef	_X86_IO_CPP_
#define	_X86_IO_CPP_

#include "X86IO.hpp"

inline void X86IO::outputByte(UINT16 port, UINT8 data){
	__asm__ __volatile__(
		"outb	%b0,	%w1"::"a"(data),"Nd"(port));
}

inline UINT8 X86IO::inputByte(UINT16 port){
	UINT8 data;
	__asm__ __volatile__(
		"inb	%w1,	%b0":"=a"(data):"Nd"(port));
	return data;
}

inline void X86IO::outputWord(UINT16 port, UINT16 data){
	__asm__ __volatile__(
		"outw	%w0,	%w1"::"a"(data),"Nd"(port));
}

inline UINT16 X86IO::inputWord(UINT16 port){
	UINT16 data;
	__asm__ __volatile__(
		"inw	%w1,	%w0":"=a"(data):"Nd"(port));
	return data;
}

inline void X86IO::outputDword(UINT16 port, UINT32 data){
	__asm__ __volatile__(
		"outl	%0,	%w1"::"a"(data),"Nd"(port));
}

inline UINT32 X86IO::inputDword(UINT16 port){
	UINT32 data;
	__asm__ __volatile__(
		"inl	%1,	%w0":"=a"(data):"Nd"(port));
	return data;
}

#endif // _X86_IO_H_
