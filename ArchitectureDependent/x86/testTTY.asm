
INCLUDE "tty.asm"

testTTY:
	push	DWORD 0F000000h
	push	.str
	call	TTY.printString
	ret

.str:	db	9, 'Print a string to test', 9, 'TTY.printString', 13, 10
	db	'_', 8, 'test printString', 13, 10
	db	'0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF'
	db	'0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF', 0
	