
#ifndef _TTY_H_
#define _TTY_H_

#include "../../DataStructure/Basic/Basic.hpp"

class TTY{
public:
	static char* const textBuffer;
	static const UINT8 COLUMNS, ROWS, TAB_LENGTH;
	UINT8 currentRow, currentColumn;
	
	CALLCONV TTY();
	
	typedef struct _Parameters{
		UINT8 row;
		UINT8 column;
		char endChar;
		UINT8 color;
	}Parameters;
	
	CALLCONV bool printString(char* str, Parameters p);
};

#endif // _TTY_H_
