


searchSystemTables:
	movzx	esi,	WORD [40Eh]
	cmp	esi,	0A000h
	jae	@F
	cmp	esi,	9000h
	jb	@F
	shl	esi,	4
	lea	edi,	[esi+1024]
	jmp	.searchLoop1
@@:	mov	esi,	9FC00h
	mov	edi,	0A0000h
.searchLoop1:
	cmp	DWORD [esi],	'RSD '
	jne	@F
	cmp	DWORD [esi+4],	'PTR '
	je	.validateRSDP
@@:	cmp	DWROD [esi],	'_MP_'
	je	.validateMP
	add	esi,	16
	cmp	esi,	edi
	jb	.searchLoop1
	
	mov	esi,	0E0000h
	mov	edi,	0F0000h
.searchLoop2:
	cmp	DWORD [esi],	'RSD '
	jne	@F
	cmp	DWORD [esi+4],	'PTR '
	je	.validateRSDP
@@:	add	esi,	16
	cmp	esi,	edi
	jb	.searchLoop2
	
	mov	esi,	0F0000h
	mov	edi,	100000h
.searchLoop3:
	cmp	DWORD [esi],	'RSD '
	jne	@F
	cmp	DWORD [esi+4],	'PTR '
	je	.validateRSDP
@@:	cmp	DWROD [esi],	'_MP_'
	je	.validateMP
	cmp	DWORD [esi],	'_SM_'
	jne	@F
	cmp	DWORD [esi+10h],	'_DMI'
	je	.validateSMBIOS
@@:	cmp	DWORD [esi],	'_SM3'
	je	.validateSMBIOS3
	
	add	esi,	16
	cmp	esi,	edi
	jb	.searchLoop3
	jmp	endSearchSystemTable

AcpiRsdp:	dd	0
AcpiRsdt:	dd	0
AcpiXsdt:	dq	0
AcpiFadt:	dq	0
AcpiMadt:	dq	0
MpFloatingPointer:	dd	0
SMBiosEntry:	dd	0
SMBios3Entry:	dd	0

endSearchSystemTable:
