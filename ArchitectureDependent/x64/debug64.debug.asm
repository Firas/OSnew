

ALIGN 2
DebugRegStr:	db	'rax: 00000000 00000000'
DebugTab:	db	9, 0
DebugNewLine:	db	13, 10, 0

; void watchGeneralRegisters();
public watchGeneralRegisters
watchGeneralRegisters:
	pushfq
	push	rcx
	push	rax
	push	rdx
	push	r8
	push	r9
	
	lea	rcx,	[DebugRegStr]
	mov	DWORD [rcx],	'rax:'
	mov	rdx,	rax
	call	.middle
	
	lea	rcx,	[DebugRegStr]
	mov	BYTE [rcx+1],	'b'
	mov	rdx,	rbx
	call	.middle
	call	.endl
	
	lea	rcx,	[DebugRegStr]
	mov	BYTE [rcx+1],	'c'
	mov	rdx,	[rsp+4*8+4*8]
	call	.middle
	
	lea	rcx,	[DebugRegStr]
	mov	BYTE [rcx+1],	'd'
	mov	rdx,	[rsp+2*8+4*8]
	call	.middle
	call	.endl
	
	lea	rcx,	[DebugRegStr]
	mov	WORD [rcx+1],	'si'
	mov	rdx,	rsi
	call	.middle
	
	lea	rcx,	[DebugRegStr]
	mov	BYTE [rcx+1],	'd'
	mov	rdx,	rdi
	call	.middle
	call	.endl
	
	lea	rcx,	[DebugRegStr]
	mov	WORD [rcx+1],	'bp'
	mov	rdx,	rbp
	call	.middle
	
	lea	rcx,	[DebugRegStr]
	mov	BYTE [rcx+1],	's'
	lea	rdx,	[rsp+7*8+4*8]
	call	.middle
	call	.endl
	
	lea	rcx,	[DebugRegStr]
	mov	DWORD [rcx],	' r8:'
	mov	rdx,	[rsp+8+4*8]
	call	.middle
	
	lea	rcx,	[DebugRegStr]
	mov	BYTE [rcx+2],	'9'
	mov	rdx,	[rsp+4*8]
	call	.middle
	call	.endl
	
	lea	rcx,	[DebugRegStr]
	mov	DWORD [rcx],	'r10:'
	mov	rdx,	r10
	call	.middle
	
	lea	rcx,	[DebugRegStr]
	mov	BYTE [rcx+2],	'1'
	mov	rdx,	r11
	call	.middle
	call	.endl
	
	lea	rcx,	[DebugRegStr]
	mov	BYTE [rcx+2],	'2'
	mov	rdx,	r12
	call	.middle	
	
	lea	rcx,	[DebugRegStr]
	mov	BYTE [rcx+2],	'3'
	mov	rdx,	r13
	call	.middle
	call	.endl
	
	lea	rcx,	[DebugRegStr]
	mov	BYTE [rcx+2],	'4'
	mov	rdx,	r14
	call	.middle
	
	lea	rcx,	[DebugRegStr]
	mov	BYTE [rcx+2],	'5'
	mov	rdx,	r15
	call	.middle
	call	.endl
	
	pop	r9
	pop	r8
	pop	rdx
	pop	rax
	pop	rcx
	popfq
	ret

.middle:
	sub	rsp,	4*8
	add	rcx,	5
	call	UnsignedInteger._toHexString
	
	lea	rcx,	[DebugRegStr]
	mov	rax,	[rcx+5+8]
	mov	[rcx+5+9],	rax
	mov	BYTE [rcx+5+8],	' '
	
	mov	rdx,	0F000000h
	mov	dx,	WORD [TTY.currentRow]
	call	TTY.printString
	
	add	rsp,	4*8
	ret
.endl:
	lea	rcx,	[DebugNewLine]
	mov	rdx,	0F000000h
	mov	dx,	WORD [TTY.currentRow]
	sub	rsp,	4*8
	call	TTY.printString
	add	rsp,	4*8
	ret
	
DebugMemStr:	db	'00000000 00000000: ', 0
DebugHex:	db	'00', 0
DebugChar:	db	' ', 0, 0
public watchMemory
watchMemory:
	push	rcx
	push	rax
	push	rdx
	push	r8
	push	r9
	push	rbx
	push	r12
	pushfq
	sub	rsp,	4*8
	
	mov	bx,	1010h
	mov	r12,	rcx
.loop0:
	mov	rdx,	r12
	lea	rcx,	[DebugMemStr]
	call	UnsignedInteger._toHexString

	; 下面将后8位数字（低8位）后移一字节
	; 一可在中间插入空格，便于看清这么长的数字
	; 二可覆盖_toHexString函数在最后加入的字符串结束符0
	; moves the 8 digits at the back backward for 1 byte
	; first, insert a space in the middle to make the long number more clear
	; second, overwrite the '\0' added by _toHexString at the end
	lea	rcx,	[DebugMemStr]
	mov	rax,	[rcx+8]
	mov	[rcx+9],	rax
	mov	BYTE [rcx+8],	' '
	
	mov	rdx,	0F000000h
	mov	dx,	WORD [TTY.currentRow]
	call	TTY.printString

	mov	bl,	16
.loop1:
	movzx	rdx,	BYTE [r12]
	lea	rcx,	[DebugHex]
	call	Unsigned8._toHexString
	mov	rdx,	0F000000h
	mov	dx,	WORD [TTY.currentRow]
	lea	rcx,	[DebugHex]
	call	TTY.printString
	
	mov	al,	[r12]
	cmp	al,	' '
	jb	.else0
	cmp	al,	127
	jae	.else0
	; 若是常规字符，蓝色背景输出
	; output normal character with blue background
	mov	[DebugChar],	al
	mov	rdx,	1F000000h
	jmp	.endif0
.else0:
	; 若是控制字符，不输出（输出黑色背景空格）
	; does not output "control character" (output space with black background)
	mov	BYTE [DebugChar],	' '
	mov	rdx,	0F000000h
.endif0:
	inc	BYTE [DebugChar+2]
	cmp	BYTE [DebugChar+2],	4
	jae	.else1
	mov	BYTE [DebugChar+1],	0
	jmp	.endif1
.else1:
	mov	WORD [DebugChar+1],	' '
.endif1:
	mov	dx,	WORD [TTY.currentRow]
	lea	rcx,	[DebugChar]
	call	TTY.printString
	
	inc	r12
	dec	bl
	jnz	.loop1
	
	mov	rdx,	0F000000h
	mov	dx,	WORD [TTY.currentRow]
	lea	rcx,	[DebugTab]
	call	TTY.printString
	mov	rdx,	0F000000h
	mov	dx,	WORD [TTY.currentRow]
	lea	rcx,	[DebugNewLine]
	call	TTY.printString
	
	sub	bx,	100h
	jg	.loop0
	
	add	rsp,	4*8
	popfq
	pop	r12
	pop	rbx
	pop	r9
	pop	r8
	pop	rdx
	pop	rax
	pop	rcx
	ret

public watchStack
watchStack:
	push	rcx
	mov	rcx,	rsp
	;add	rcx,	2*8	; rbp and ret
	call	watchMemory
	pop	rcx
	ret

INCLUDE "DataStructure\Basic\Unsigned8.asm"
INCLUDE "DataStructure\Basic\UnsignedInteger.asm"
INCLUDE "tty.asm"
