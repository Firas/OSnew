#include <DataStructure/Basic/UnsignedInteger.cpp>

extern "C" ULONG getPageTableEntry(void *addr);

bool isFree(void *addr){
	ULONG pagingEntryPointer = getPageTableEntry(addr);
	ULONG pagingEntry = *reinterpret_cast<ULONG*>(pagingEntryPointer & ~3);
	return (pagingEntry & 0x801) == 1;
}
