

IF ~DEFINED _UNSIGNED_8_H_
_UNSIGNED_8_H_	EQU	1

; void Unsigned8::_toHexString(char* buffer, unsigned i);
Unsigned8._toHexString:
	test	rcx,	rcx
	jz	.return
	mov	BYTE [rcx+2],	0
	mov	r8b,	dl
	and	r8b,	0Fh	; low 4 bit of DL
	shr	dl,	4	; high 4 bit of DL
	or	dl,	'0'
	cmp	dl,	'9'
	jna	@F
	add	dl,	'A'-'9'-1
@@:	mov	[rcx],	dl
	or	r8b,	'0'
	cmp	r8b,	'9'
	jna	@F
	add	r8b,	'A'-'9'-1
@@:	mov	[rcx+1],	r8b
.return:
	ret
	
END IF ; _UNSIGNED_8_H_
