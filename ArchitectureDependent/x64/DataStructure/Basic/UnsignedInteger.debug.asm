; 
; 

IF ~DEFINED _UNSIGNED_INTEGER_H_
_UNSIGNED_INTEGER_H_	EQU	1

UnsignedInteger.MAX	EQU	0FFFFFFFFFFFFFFFFh

; bool UnsignedInteger::parseStringANSI(char* str, UINT8 radix, unsigned *output);
UnsignedInteger.parseStringANSI:
	
	ret
	
; bool UnsignedInteger::toStringANSI(char *buffer, int i, unsigned radix);
UnsignedInteger.toStringANSI:
	
	ret
	
; void UnsignedInteger::_toHexString(char* buffer, unsigned i);
UnsignedInteger._toHexString:
	test	rcx,	rcx
	jz	.return
	add	rcx,	16
	mov	BYTE [rcx],	0
	mov	r9,	7
.loop0:
	dec	rcx
	mov	r8b,	dl
	and	r8b,	0Fh	; low 4 bits
	or	r8b,	'0'
	cmp	r8b,	'9'
	jna	@F
	add	r8b,	'A'-'9'-1
@@:	mov	[rcx],	r8b
	shr	dl,	4	; high 4 bits
	or	dl,	'0'
	cmp	dl,	'9'
	jna	@F
	add	dl,	'A'-'9'-1
@@:	dec	rcx
	mov	[rcx],	dl
	test	r9,	r9
	jz	.return
	dec	r9
	shr	rdx,	8
	jmp	.loop0
.return:
	ret

END IF ; ~DEFINED _UNSIGNED_INTEGER_H_
