

testTTY:
	sub	rsp,	4*8
	mov	edx,	0F000000h
	lea	rcx,	[testStr]
	call	TTY.printString
	add	rsp,	4*8
	ret

testStr:
	db	9, 'Print a string to test', 9, 'TTY.printString', 13, 10
	db	'_', 8, 'test printString', 13, 10
	db	'0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF'
	db	'0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF', 0
