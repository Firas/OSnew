FORMAT MS64 COFF

INCLUDE "../LegacyBIOS/loader.asm"
INCLUDE "../x86/cpuid.asm"
INCLUDE "PageIA32e.asm"

DISPLAY_OFFSET	readDiskInReal
	mov	rcx,	MEM_MAP_ADDR
	call	watchMemory
	
	call	quitLongMode
USE32
	call	disableProtectedMode
USE16	
	mov	ax,	41h
	mov	dl,	[BootDriveNum]
	int	13h
	
	call	far 0:enableProtectedMode32
USE32
	call	enterLongMode
	
USE64
	movzx	rax,	WORD [40Eh]
	movzx	rcx,	WORD [413h]
	call	watchGeneralRegisters
	
	mov	rcx,	20000h
	call	getPageTableEntry
	jmp	die
	
INCLUDE "debug64.asm"
