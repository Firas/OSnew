

; void searchSystemTables(void);
DISPLAY_OFFSET	searchSystemTables
	push	rsi
	push	rdi
	push	rbx
	
	; 开始搜索：先搜索拓展BIOS数据区（EBDA），然后搜索ROM（0E0000h~0EFFFFh，0F0000h~0FFFFFh）。
	; 一般来说EBDA存在的话，它的首地址可在内存40Eh处找到（[40Eh]:0）。
	; ACPI的RSDP可能在的两个区域：EBDA的首1KB区域，或者内存0E0000h~0FFFFFh。
	; MP Floating Pointer可能在的三个区域：EBDA的首1KB区域，若EBDA段未定义则可能在系统基本内存的最后1KB
	; （可在内存413h处找到系统基本内存的大小），或者内存0F0000h~0FFFFFh。
	; SMBIOS入口点结构可能在的区域：内存0F0000h~0FFFFFh。
	
	; Start searching: search extended BIOS data area (EBDA) first, and then search ROM
	; (0E0000h~0EFFFFh，0F0000h~0FFFFFh).
	; Normally the address of EBDA can be found in a 2-byte location (40Eh) if EBDA exists
	; (the address of EBDA is [40Eh]:0).
	; The areas in which ACPI RSDP may exist: the first 1 KB area of EBDA, the lask 1 KB of
	; system base memory if the EBDA segment is undefined, BIOS ROM address between 0E0000h and 0FFFFFh.
	; The areas in which MP Floating Pointer may exist: the first 1 KB area of EBDA,
	; BIOS ROM address between 0F0000h and 0FFFFFh.
	; The area in which SMBIOS entry points may exist: BIOS ROM address between 0F0000h and 0FFFFFh.
	mov	r8,	'RSD PTR '
	mov	r9d,	'_MP_'
	mov	ecx,	'_SM_'
	mov	edx,	'_SM3'
	
	lea	rsi,	[.loop0Increment]
	mov	[.jumpAddr],	rsi
	movzx	rsi,	WORD [40Eh]
	cmp	rsi,	09000h
	jb	.invalidEbdaSegment
	cmp	rsi,	09FC0h
	ja	.invalidEbdaSegment
	shl	rsi,	4
	lea	rdi,	[rsi+1024]
	jmp	.loop0
.invalidEbdaSegment:
	movzx	rdi,	WORD [413h]	; system base memory in KB
	shl	rdi,	10		; X 1024
	lea	rsi,	[rdi-1024]
	
.loop0:
	cmp	r8,	[rsi]
	je	.validateRsdp
	cmp	r9d,	[rsi]
	je	.validateMpFp
.loop0Increment:
	add	rsi,	16
	cmp	rsi,	rdi
	jb	.loop0
	
	lea	rsi,	[.loop1Increment]
	mov	[.jumpAddr],	rsi
	mov	rsi,	0E0000h
	mov	rdi,	0F0000h
.loop1:
	cmp	r8,	[rsi]
	je	.validateRsdp
.loop1Increment:
	add	rsi,	16
	cmp	rsi,	rdi
	jb	.loop1
	
	lea	rsi,	[.loop2Increment]
	mov	[.jumpAddr],	rsi
	mov	rsi,	0F0000h
	mov	rdi,	100000h
.loop2:
	cmp	r8,	[rsi]
	je	.validateRsdp
	cmp	r9d,	[rsi]
	je	.validateMpFp
	cmp	ecx,	[rsi]
	je	.validateSMBios
	cmp	edx,	[rsi]
	je	.validateSMBios3
.loop2Increment:
	add	rsi,	16
	cmp	rsi,	rdi
	jb	.loop2
	
	call	searchAcpiTables
	pop	rbx
	pop	rdi
	pop	rsi
	ret

.validateRsdp:
	lea	rbx,	[rsi+8]
	mov	r10b,	20-8
	mov	al,	1Fh	; 52h+53h+44h+20h+50h+54h+52h+20h=21Fh
@@:	add	al,	[rbx]
	inc	rbx
	dec	r10b
	jnz	@B
	test	al,	al
	jnz	.invalidRsdp
	mov	[AcpiRsdp],	rsi
	
	mov	r10d,	[rbx]
	cmp	r10d,	1000h
	ja	.invalidRsdp
	sub	r10d,	20
	jbe	.invalidRsdp
@@:	add	al,	[rbx]
	inc	rbx
	dec	r10d
	jnz	@B
	test	al,	al
	jnz	.invalidRsdp
	mov	rbx,	[rsi+24]
	cmp	DWORD [rbx],	'XSDT'
	jne	.invalidRsdp
	mov	[AcpiXsdt],	rbx
.invalidRsdp:
	jmp	QWORD [.jumpAddr]

.validateMpFp:
	mov	rbx,	rsi
	movzx	r10d,	BYTE [rsi+8]
	shl	r10d,	4	; length = paragraphs * 16
	xor	al,	al
@@:	add	al,	[rbx]
	inc	rbx
	dec	r10d
	jnz	@B
	test	al,	al
	jnz	@F
	mov	[MpFloatingPointer],	rsi
@@:	jmp	QWORD [.jumpAddr]

.validateSMBios:
	cmp	DWORD [rsi],	'_DMI'
	jne	.invalidSMBios
	mov	rbx,	rsi
	mov	r10b,	[rsi+5]
	xor	al,	al
@@:	add	al,	[rbx]
	inc	rbx
	dec	r10
	jnz	@B
	test	al,	al
	jnz	.invalidSMBios
	mov	[SMBiosEntry],	rsi
.invalidSMBios:
	jmp	QWORD [.jumpAddr]
.validateSMBios3:
	mov	rbx,	rsi
	mov	r10b,	[rsi+6]
	xor	al,	al
@@:	add	al,	[rbx]
	inc	rbx
	dec	r10
	jnz	@B
	test	al,	al
	jnz	@F
	mov	[SMBios3Entry],	rsi
@@:	jmp	QWORD [.jumpAddr]
	
.jumpAddr:		dq	.loop0Increment

searchAcpiTables:
	mov	rdx,	[AcpiXsdt]
	test	rdx,	rdx
	jz	.searchRsdt
	mov	ecx,	[rdx+4]		; length of XSDT
	sub	ecx,	36
	add	rdx,	36
.loop0:
	mov	rax,	[rdx]
	cmp	DWORD [rax],	'FADT'
	jne	@F
	mov	[AcpiFadt],	rax
	jmp	.loop0Increment
@@:	cmp	DWORD [rax],	'APIC'
	jne	.loop0Increment
	mov	[AcpiMadt],	rax
.loop0Increment:
	add	rdx,	8
	sub	ecx,	8
	ja	.loop0
	ret
.searchRsdt:
	mov	rdx,	[AcpiRsdp]	; address of RSDP
	test	rdx,	rdx
	jnz	@F
	ret
@@:	mov	edx,	[rdx+16]	; address of RSDT
	mov	ecx,	[rdx+4]		; length of RSDT
	sub	ecx,	36
	add	rdx,	36
	xor	rax,	rax
.loop1:
	mov	eax,	[rdx]
	cmp	DWORD [rax],	'FADT'
	jne	@F
	mov	DWORD [AcpiFadt],	eax
	jmp	.loop1Increment
@@:	cmp	DWORD [rax],	'APIC'
	jne	.loop1Increment
	mov	DWORD [AcpiMadt],	eax
.loop1Increment:
	add	rdx,	4
	sub	ecx,	4
	ja	.loop1
	ret

ALIGN 8
; ACPI Root System Description Pointer
AcpiRsdp:	dq	0
; ACPI Extended Root System Description Table
AcpiXsdt:	dq	0
; Fixed ACPI Description Table
AcpiFadt:	dq	0
; ACPI Multiple APIC Desciptor Table
AcpiMadt:	dq	0

; 
MpFloatingPointer:	dq	0

; 
SMBiosEntry:	dq	0
SMBios3Entry:	dq	0
