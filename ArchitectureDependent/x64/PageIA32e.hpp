/***
 * IA-32e分页数据结构
 * IA-32e Paging Structure
 */

#ifndef	_PAGE_IA32E_H_
#define	_PAGE_IA32E_H_	1

// 页表项 PTE
typedef union _PageTableEntry{
	void *addr;
	struct{
		UINT64 present:1;
		UINT64 r_w:1;
		UINT64 u_s:1;
		UINT64 writeThrough:1;
		UINT64 cacheDisable:1;
		UINT64 accessed:1;
		UINT64 dirty:1;
		UINT64 attributeTable:1;
		UINT64 global:1;
		UINT64 ignored:3;
		UINT64 address:51;
		UINT64 executeDisable:1;
	}flags;
}PageTableEntry;

// 页目录项 PDE
typedef union _PageDirectoryEntry{
	void *addr;
	struct{
		UINT64 present:1;
		UINT64 r_w:1;
		UINT64 u_s:1;
		UINT64 writeThrough:1;
		UINT64 cacheDisable:1;
		UINT64 accessed:1;
		UINT64 dirty:1;
		UINT64 bigPage:1;
		UINT64 global:1;
		UINT64 ignored:3;
		UINT64 attributeTable:1;
		UINT64 address:50;
		UINT64 executeDisable:1;
	}flags;
}PageDirectoryEntry;

// 页目录指针表项 PDPTE
typedef union _PageDirPointerEntry{
	void *addr;
	struct{
		UINT64 present:1;
		UINT64 r_w:1;
		UINT64 u_s:1;
		UINT64 writeThrough:1;
		UINT64 cacheDisable:1;
		UINT64 accessed:1;
		UINT64 dirty:1;
		UINT64 bigPage:1;
		UINT64 global:1;
		UINT64 ignored:3;
		UINT64 attributeTable:1;
		UINT64 address:50;
		UINT64 executeDisable:1;
	}flags
}PageDirPointerEntry;

// PML4表项
typedef union _PML4Entry{
	PageDirPointerEntry *addr;
	struct{
		UINT64 present:1;
		UINT64 r_w:1;
		UINT64 u_s:1;
		UINT64 writeThrough:1;
		UINT64 cacheDisable:1;
		UINT64 accessed:1;
		UINT64 address:57;
		UINT64 executeDisable:1;
	}flags;
}PML4Entry;

// 页面错误错误码
// Page Fault Code
typedef union _PageFaultCode{
	UINT64 value;
	struct{
		UINT64 present:1;
		UINT64 w_r:1;
		UINT64 u_s:1;
		UINT64 revb:1;
		UINT64 i_d:1;
	}flags;
class PageIA32e{
public:
	const UINT64 PAGE_SIZE0 = 0x100, PAGE_SIZE1 = 0x200000, PAGE_SIZE2 = 0x40000000;
	
}; // class PageIA32e
