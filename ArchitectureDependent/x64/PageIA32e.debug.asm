; IA-32e分页数据结构
; IA-32e Paging Structure

IF ~DEFINED _PAGE_IA32E_H_
_PAGE_IA32E_H_	EQU	1

; PML4表项
PML4E.PRESENT	EQU	1
PML4E.R_W	EQU	2
PML4E.U_S	EQU	4
PML4E.PWT	EQU	8
PML4E.PCD	EQU	10h
PML4E.ACCESSED	EQU	20h
PML4E.ADDR	EQU	7FFFFFFFFFFFF000h
PML4E.XD	EQU	8000000000000000h

; 页目录指针表项 PDPTE
PDPTE.PRESENT	EQU	1
PDPTE.R_W	EQU	2
PDPTE.U_S	EQU	4
PDPTE.PWT	EQU	8
PDPTE.PCD	EQU	10h
PDPTE.ACCESSED	EQU	20h
PDPTE.DIRTY	EQU	40h
PDPTE.PAGE_SIZE	EQU	80h
PDPTE.GLOBAL	EQU	100h
PDPTE.PAT	EQU	1000h
PDPTE.PD_ADDR	EQU	7FFFFFFFFFFFF000h
PDPTE.FRAME_ADDR	EQU	7FFFFFFFC0000000h
PDPTE.XD	EQU	8000000000000000h

; 页目录项 PDE
PDE.PRESENT	EQU	1
PDE.R_W		EQU	2
PDE.U_S		EQU	4
PDE.PWT		EQU	8
PDE.PCD		EQU	10h
PDE.ACCESSED	EQU	20h
PDE.DIRTY	EQU	40h
PDE.PAGE_SIZE	EQU	80h
PDE.GLOBAL	EQU	100h
PDE.PAT		EQU	1000h
PDE.PT_ADDR	EQU	7FFFFFFFFFFFF000h
PDE.FRAME_ADDR	EQU	7FFFFFFFFFC00000h
PDE.XD		EQU	8000000000000000h

; 页表项 PTE
PTE.PRESENT     EQU	1
PTE.R_W		EQU	2
PTE.U_S		EQU	4
PTE.PWT		EQU	8
PTE.PCD		EQU	10h
PTE.ACCESSED	EQU	20h
PTE.DIRTY	EQU	40h
PTE.PAT		EQU	80h
PTE.GLOBAL	EQU	100h
PTE.ADDR	EQU	0FFFFF000h
PTE.XD		EQU	8000000000000000h

; 页面错误错误码
PAGEFAULT.PRESENT	EQU	1
PAGEFAULT.W_R		EQU	2
PAGEFAULT.U_S		EQU	4
PAGEFAULT.RSVB		EQU	8
PAGEFAULT.I_D		EQU	10h

INCLUDE "../x86/MSR.asm"

initPageIA32e:
checkLongModeSupport:
	or	DWORD [CPUID_EXT1_EDX],	CPUID.X64
	jnz	emptyPagingStructures
	
	jmp	die

emptyPagingStructures:
	cld
	mov	edi,	PAGE_STRUC1
	mov	ecx,	4*1024
	xor	eax,	eax
	rep	stosd

setFirstPML4Entry:
	mov	edi,	PAGE_STRUC1
	mov	DWORD [edi],	PAGE_STRUC2+PML4E.PRESENT+PML4E.R_W
	mov	cr3,	edi

setFirstPDPTEntry:
	mov	edi,	PAGE_STRUC2
	mov	DWORD [edi],	PAGE_STRUC3+PDPTE.PRESENT+PDPTE.R_W

setFirstPageDirEntry:
	mov	edi,	PAGE_STRUC3
	mov	DWORD [edi],	PAGE_STRUC4+PDE.PRESENT+PDE.R_W
	
setFirstPageTableEntries:
	mov	edi,	PAGE_STRUC4
	mov	esi,	0A0000h
	mov	eax,	PTE.PRESENT+PTE.R_W
.loop0:
	mov	[edi],	eax
	add	edi,	8
	add	eax,	4*1024
	cmp	eax,	esi
	jbe	.loop0
	
	mov	edi,	PAGE_STRUC4+0A0000h/1000h*8
	mov	esi,	100000h
	mov	eax,	0A0000h+PTE.PRESENT+PTE.R_W+PTE.PWT+PTE.PCD
.loop1:
	mov	[edi],	eax
	add	edi,	8
	add	eax,	4*1024
	cmp	eax,	esi
	jbe	.loop1
	
	cli
	call	enablePageIA32e
	call	enableLongMode
	sti
	jmp	far (GdtStart.codeLong-GdtStart):endInitPageIA32e
	
INCLUDE "protectedMode64.asm"

enablePageIA32e:
	mov	eax,	cr4
	or	eax,	CR4.PAE
	mov	cr4,	eax
	
	mov	ecx,	IA32_EFER
	rdmsr
	or	eax,	IA32_EFER.LME
	wrmsr
	
	mov	edx,	cr0
	or	edx,	CR0.PG
	mov	cr0,	edx
	ret
	
USE64
disablePageIA32e:
	mov	rax,	cr0
	and	eax,	NOT CR0.PG
	mov	cr0,	rax
	ret
	
; PML4E* getPML4Entry(void *address);
public getPML4Entry
getPML4Entry:
	mov	rax,	cr3
	and	rax,	0FFFFFFFFFFFFF000h
	
	shr	rcx,	39
	lea	rax,	[rax+4*rcx]
	ret

; PDPTE* getPDPTEntry(void *address);
public getPDPTEntry
getPDPTEntry:
	mov	rax,	cr0
	and	rax,	0FFFFFFFFFFFFF000h
	
	mov	rdx,	rcx
	shr	rcx,	39
	lea	rax,	[rax+4*rcx]
	; 先右移39位再乘以4，这样不需要将低2位清0
	
	test	QWORD [rax],	PML4E.PRESENT
	jnz	@F
	
	; 内存区域的存在位为0
	or	rax,	11b
	; 标记此项为PML4表中的对应1GB内存区域的项
	ret
@@:	mov	rax,	[rax]
	and	rax,	0FFFFFFFFFFFFF000h
	
	mov	rcx,	rdx
	shr	rcx,	30
	and	rcx,	3FFh
	lea	rax,	[rax+4*rcx]
	ret
	
; PDE* getPageDirEntry(void *address);
public getPageDirEntry
getPageDirEntry:
	mov	rax,	cr3
	and	rax,	0FFFFFFFFFFFFF000h
	
	mov	rdx,	rcx
	shr	rcx,	39
	lea	rax,	[rax+4*rcx]
	; 先右移39位再乘以4，这样不需要将低2位清0
	
	test	QWORD [rax],	PML4E.PRESENT
	jnz	@F
	
	; 内存区域的存在位为0
	or	rax,	11b
	; 标记此项为PML4表中的对应1GB内存区域的项
	ret
@@:	mov	rax,	[rax]
	and	rax,	0FFFFFFFFFFFFF000h
	
	mov	rcx,	rdx
	shr	rcx,	30
	and	rcx,	3FFh
	lea	rax,	[rax+4*rcx]
	
	test	QWORD [rax],	PDPTE.PRESENT
	jnz	@F
	
	; 内存区域的存在位为0
	or	rax,	10b
	; 标记此项为PDPTE表中的对应1GB内存区域的项
	ret
@@:	test	QWORD [rax],	PDPTE.PAGE_SIZE
	jz	@F
	
	; 1GB页面
	or	rax,	10b
	; 标记此项为PDPTE表中的对应1GB内存区域的项
	ret
	
@@:	mov	rax,	[rax]
	and	rax,	0FFFFFFFFFFFFF000h
	
	mov	rcx,	rdx	; 恢复地址
	shr	rcx,	21
	and	rcx,	3FFh
	lea	rax,	[rax+4*rcx]
	ret
	
; PTE* getPageTableEntry(void *address);
public getPageTableEntry
getPageTableEntry:
	mov	rax,	cr3
	and	rax,	0FFFFFFFFFFFFF000h
	
	mov	rdx,	rcx	; 保存地址
	shr	rcx,	39
	lea	rax,	[rax+4*rcx]
	; 先右移39位再乘以4，这样不需要将低2位清0
	
	test	QWORD [rax],	PML4E.PRESENT
	jnz	@F
	
	; 内存区域的存在位为0
	or	rax,	11b
	; 标记此项为PML4表中的对应256GB内存区域的项
	ret
@@:	mov	rax,	[rax]
	and	rax,	0FFFFFFFFFFFFF000h
	
	mov	rcx,	rdx	; 恢复地址
	shr	rcx,	30
	and	rcx,	3FFh
	lea	rax,	[rax+4*rcx]
	
	test	QWORD [rax],	PDPTE.PRESENT
	jnz	@F
	
	; 内存区域的存在位为0
	or	rax,	10b
	; 标记此项为PDPTE表中的对应1GB内存区域的项
	ret
@@:	mov	rax,	[rax]
	and	rax,	0FFFFFFFFFFFFF000h
	
	mov	rcx,	rdx	; 恢复地址
	shr	rcx,	21
	and	rcx,	3FFh
	lea	rax,	[rax+4*rcx]
	
	test	QWORD [rax],	PDE.PRESENT
	jnz	@F
	
	; 内存区域的存在位为0
	or	rax,	1
	; 标记此项为页目录表中的对应2MB内存区域的项
	ret
@@:	test	QWORD [rax],	PDE.PAGE_SIZE
	jz	@F
	
	; 2MB页面
	or	rax,	10b
	; 标记此项为页目录表中的对应2MB内存区域的项
	ret
	
@@:	mov	rax,	[rax]
	and	rax,	0FFFFFFFFFFFFF000h
	
	mov	rcx,	rdx	; 恢复地址
	shr	rcx,	12
	and	rcx,	3FFh
	lea	rax,	[rax+4*rcx]
	ret
	
endInitPageIA32e:

END IF ; _PAGE_IA32E_H_
