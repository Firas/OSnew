DISPLAY_OFFSET	initProtectedMode32
	xor	ax,	ax
	mov	ds,	ax
	mov	dx,	IDT_ADDR/16
	mov	es,	dx
	mov	di,	IDT_ADDR AND 0Fh
	
	mov	eax,	DWORD [Idt.exception]
	mov	edx,	DWORD [Idt.exception+4]
	; int 0�������㣨����
	; int 0: Divide Error Exception (#DE) (Fault)
	; int 1�������쳣������/����
	; int 1: Debug Exception (#DB) (Trap/Fault)
	; int 2�����������жϣ��жϣ�
	; int 2: Non-maskable Interrupt (Interrupt)
	
	; int 5�������߽緶Χ������
	; int 5: BOUND Range Exceeded Exception (#BR) (Fault)
	; int 6���Ƿ������루����
	; int 6: Invalid Opcode Exception (#UD) (Fault)
	; int 7������ѧЭ������������
	; int 7: Device Not Available Exception (#NM) (Fault)
	; int 8��˫�ش�����ֹ��
	; int 8: Double Fault Exception (#DF) (Abort)
	
	; int 10���Ƿ�TSS������
	; int 10: Invalid TSS Exception (#TS) (Fault)
	; int 11���β����ڣ�����
	; int 11: Segment Not Present (#NP) (Fault)
	; int 12����ջ�δ��󣨴���
	; int 12: Stack Fault Exception (#SS) (Fault)
	; int 13��һ�㱣���쳣������
	; int 13: General Protection Exception (#GP) (Fault)
	; int 14��ҳ����󣨴���
	; int 14: Page-Fault Exception (#PF) (Fault)
	
	; int 16��x87 FPU������󣨴���
	; int 16: x87 FPU Floating-Point Error (#MF) (Fault)
	; int 17����������󣨴���
	; int 17: Alignment Check Exception (#AC) (Fault)
	; int 18��������������ֹ��
	; int 18: Machine-Check Exception (#MC) (Fault)
	; int 19��SIMD�����쳣������
	; int 19: SIMD Floating-Point Exception (#XM) (Fault)
	mov	cx,	19+1
.loop0:
	mov	[es:di],	eax
	mov	[es:di+4],	edx
	add	di,	8
	loop	.loop0
	
	mov	eax,	DWORD [Idt.trap]
	mov	edx,	DWORD [Idt.trap+4]
	mov	di,	IDT_ADDR AND 0Fh
	; int 3���ϵ�����
	; int 3: Breakpoint Exception (#BP) (Trap)
	; int 4���������
	; int 4: Overflow Exception (#OF) (Trap)
	mov	[es:di+3*8],	eax
	mov	[es:di+3*8+4],	edx
	mov	[es:di+4*8],	eax
	mov	[es:di+4*8+4],	edx

	mov	eax,	DWORD [Idt.picMaster]
	mov	edx,	DWORD [Idt.picMaster+4]
	mov	di,	(IDT_ADDR AND 0Fh)+30h*8
	mov	cx,	8
.loop1:
	mov	[es:di],	eax
	mov	[es:di+4],	edx
	add	di,	8
	loop	.loop1
	
	mov	eax,	DWORD [Idt.picSlave]
	mov	edx,	DWORD [Idt.picSlave+4]
	mov	cx,	8
.loop2:
	mov	[es:di],	eax
	mov	[es:di+4],	edx
	add	di,	8
	loop	.loop2
	
	; reserved
	xor	eax,	eax
	mov	di,	IDT_ADDR AND 0Fh
	mov	[es:di+9*8],	eax
	mov	[es:di+9*8+4],	eax
	mov	[es:di+15*8],	eax
	mov	[es:di+15*8+4],	eax
	
	add	di,	20*8
	mov	cx,	4*(30h-20)
	rep	stosw
	
	add	di,	16*8
	mov	cx,	4*(256-40h)
	rep	stosw
	
	call	far 0:enableProtectedMode32
USE32
	jmp	protectedMode32Enabled
	
USE16

INCLUDE "A20.asm"
INCLUDE "../x86/NMI.asm"
INCLUDE "../x86/PIC8259.asm"
INCLUDE "../x86/ControlRegister.asm"

USE16
DISPLAY_OFFSET	enableProtectedMode32
	xor	ax,	ax
	mov	ds,	ax
	call	enableA20
	cli
	call	disableNMI
	call	disablePIC8259
	
	xor	al,	al
	out	0F0h,	al	; reset IGNNE# if asserted in the FPU
	out	0F1h,	al
	
	sidt	FWORD [IDTR16]
	lgdt	FWORD [GdtStart+2]
	lidt	FWORD [IDTR32]
	
	mov	BYTE [GdtStart.tssKernel+5],	10001001b
	
	xor	eax,	eax
	xor	edx,	edx
	pop	ax
	pop	dx
	shl	edx,	4
	add	edx,	eax
	mov	[ProtectedMode32Entry],	edx
	
	mov	eax,	cr0
	or	al,	CR0.PE		; Protected Mode Enable
	and	eax,	NOT (CR0.NW OR CR0.CD)	; Enable Cache
	mov	cr0,	eax
	
	db	66h, 0EAh
	dd	enableProtectedMode32.protected
	dw	(GdtStart.codeAll-GdtStart)
USE32
.protected:
	mov	ax,	GdtStart.dataAll-GdtStart
	mov	ss,	ax
	mov	ds,	ax
	mov	es,	ax
	mov	fs,	ax
	mov	gs,	ax
	ltr	WORD [TR32]
	sti
	jmp	DWORD [ProtectedMode32Entry]

DISPLAY_OFFSET	ProtectedMode32Entry
	dd	0

ALIGN 8
TR16:		dw	0
IDTR16:		dw	7FFh
		dd	0

TR32:		dw	GdtStart.tssKernel-GdtStart
IDTR32:		dw	7FFh
		dd	IDT_ADDR

GdtStart:
	dw	0, GdtEnd-GdtStart-1
	dd	GdtStart
.codeAll:
	dw	0FFFFh, 0
	db	0, 10011010b, 11001111b, 0
	; 32-bit Code, Readable, Noncomforming, Not Acccessed
.dataAll:
	dw	0FFFFh, 0
	db	0, 10010010b, 11001111b, 0
	; 32-bit Data, Writable, Not Accessed
.tssKernel:
	dw	KernelTssEnd-KernelTss-1, KernelTss
	db	0, 10001001b, 01000000b, 0
	; 32-bit TSS
.codeShort:
	dw	0FFFFh, 0
	db	0, 10011010b, 0, 0
	; 16-bit Code, Readable, Noncomforming, Not Accessed
.dataShort:
	dw	0FFFFh,	0
	db	0, 10010010b, 0, 0
	; 16-bit Data, Writable, Not Accessed
.codeLong:
	dq	0
.tssLong:
	dq	0, 0
.blank:
	dq	(32-(GdtStart.blank-GdtStart)/8) dup(0)
GdtEnd:

Idt:
.exception:
	dw	die, GdtStart.codeAll-GdtStart
	db	0, 10001110b
	dw	0
	; present, DPL=0, 32-bit interrupt gate
.trap:
	dw	NullCpuInterrupt, GdtStart.codeAll-GdtStart
	db	0, 10001111b
	dw	0
	; present, DPL=0, 32-bit trap gate
.picMaster:
	dw	Null8259MasterInterrupt, GdtStart.codeAll-GdtStart
	db	0, 10001110b
	dw	0
	; present, DPL=0, 32-bit interrupt gate
.picSlave:
	dw	Null8259SlaveInterrupt, GdtStart.codeAll-GdtStart
	db	0, 10001110b
	dw	0
	; present, DPL=0, 32-bit interrupt gate

KernelTss:
.link:	dw	0, 0	; ǰһ��������
.esp0:	dd	0
.ss0:	dw	0, 0
.esp1:	dd	0
.ss1:	dw	0, 0
.esp2:	dd	0
.ss2:	dw	0, 0

.cr3:	dd	0
.eip:	dd	0

.efl:	dd	0

.eax:	dd	0
.ecx:	dd	0
.edx:	dd	0
.ebx:	dd	0

.esp:	dd	0
.ebp:	dd	0
.esi:	dd	0
.edi:	dd	0

.es:	dw	0, 0
.cs:	dw	0, 0
.ss:	dw	0, 0
.ds:	dw	0, 0
.fs:	dw	0, 0
.gs:	dw	0, 0
.ldt:	dw	0, 0
.iomap:	dw	0, 0FFFFh	; I/O map base address; no I/O map
KernelTssEnd:

USE32
Null8259SlaveInterrupt:
	push	ax
	mov	al,	PIC_EOI
	out	PIC2_COMMAND,	al
	out	PIC1_COMMAND,	al
	pop	ax
	iret
Null8259MasterInterrupt:
	push	ax
	mov	al,	PIC_EOI
	out	PIC1_COMMAND,	al
	pop	ax
NullCpuInterrupt:
	iret
	
	
disableProtectedMode:
	cli
	call	disableNMI
	call	disablePIC8259
	lidt	FWORD [IDTR16]
	
	pop	eax
	mov	[RealModeEntry],	ax
	xor	ax,	ax
	shr	eax,	4
	mov	[RealModeEntry+2],	ax
	
	jmp	far (GdtStart.codeShort-GdtStart):disableProtectedMode.code16
USE16
.code16:
	mov	ax,	GdtStart.dataShort-GdtStart
	mov	ss,	ax
	mov	ds,	ax
	mov	es,	ax
	
	mov	eax,	cr0
	and	eax,	NOT 80000001h
	; Ҫ�ڹرձ���ģʽ��ͬʱ�رշ�ҳ����������
	; Paging should be disabled at the same time as protected mode is disabled
	mov	cr0,	eax
	jmp	far 0:disableProtectedMode.realCode
.realCode:
	xor	ax,	ax
	mov	ss,	ax
	mov	ds,	ax
	mov	es,	ax
	
	jmp	DWORD [RealModeEntry]

RealModeEntry:		dw	0, 0FFFFh

protectedMode32Enabled:
	