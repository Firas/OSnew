
#ifndef	_MEMORY_PLAN_H_
#define	_MEMORY_PLAN_H_	1

#define	CPUID_ADDR	0x18000

// Int 13/AH=48h: http://www.ctyme.com/intr/rb-0715.htm
// 65-byte buffer for drive parameters
#define	DRIVE_PARAM_ADDR	0x18180

// Int 10/AX=4F01h: http://www.ctyme.com/intr/rb-0274.htm
// 256-byte buffer for mode information
#define	SVGA_MODE_INFO	0x18200
// Int 10/AX=4F00h: http://www.ctyme.com/intr/rb-0273.htm
// 512-byte buffer for SuperVGA information
#define	SVGA_INFO	0x18300

#define	MEMORY_STRUCTURE	0x18800
#define	KERNEL_MEMORY_RECORD0	0x19000

#define	IDT_ADDR	0x1A000

#define	PAGE_STRUC1	0x1B000
#define	PAGE_STRUC2	0x1C000
#define	PAGE_STRUC3	0x1D000
#define	PAGE_STRUC4	0x1E000


#define	MEM_MAP_ADDR	0x20000

#endif	// _MEMORY_PLAN_H_
