; A20 address line

enableA20:
	mov	ax,	2401h
	int	15h
	jc	.Port92h
	ret
.Port92h:
	in	al,	92h
	or	al,	2	; setting bit 1
	and	al,	0FEh	; setting bit 0 causes a reset
	out	92h,	al
	call	checkA20.no2402h
	test	al,	al
	jz	.error
	clc
	ret
.error:
	mov	si,	.tip
	jmp	printError16
.tip:	db	"An error occurs when enabling A20", 0

disableA20:
	mov	ax,	2400h
	int	15h
	jc	.Port92h
	ret
.Port92h:
	in	al,	92h
	and	al,	NOT 3	; setting bit 0 causes a reset
	out	92h,	al
	call	checkA20.no2402h
	cmp	al,	1
	jne	.error
	clc
	ret
.error:
	mov	si,	.tip
	jmp	printError16
.tip:	db	"An error occurs when disabling A20", 0

checkA20:
	mov	ax,	2402h
	int	15h
	jc	.no2402h
	ret
.no2402h:
	push	ds
	push	es
	xor	ax,	ax
	mov	ds,	ax
	mov	ax,	0FF00h
	mov	es,	ax
	not	WORD [7EFEh]
	cmp	WORD [es:8EFEh],	55AAh
	jne	.open
	not	WORD [7EFEh]
	cmp	WORD [es:8EFEh],	0AA55h
	jne	.open
	mov	al,	0
	jmp	@F
.open:
	mov	al,	1
@@:	pop	es
	pop	ds
	clc
	ret
