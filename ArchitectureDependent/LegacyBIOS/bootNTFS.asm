; FAT32活动分区引导记录

; FAT32 Volumn Boot Record

INCLUDE "../../Macro.asm"

ORG 7C00h
USE16
VbrNtfs:
	jmp	CodeStart
	nop
.fileSystemId:		db	'NTFS    '
.bytesSector:		dw	2
.sectorsCluster:	db	1
.reservedSectors:	dw	0
.reserved0:		db	5 dup(0)
.mediaDescriptor:	db	0F8h
.unused0:		db	0, 0
.sectorsTrack:		dw	63
.heads:			dw	255
.hiddenSectors:		dd	0
.unused1:		db	4 dup(0)
.reserved1:		db	80h, 0, 80h, 0
.totalSectors:		dq	0
.startClusterMFT:	dq	0
.startClusterMFTmirr:	dq	0
.clustersFileRecord:	dd	1
.clustersIndex:		dd	1
.serialNumber:		dq	0
.checkSum:		dd	0

DISPLAY_OFFSET	CodeStart
	mov	eax,	[VbrNtfs.hiddenSectors]
	inc	eax
	jmp	far 0:readOtherLoader
	
INCLUDE	"loadLoader.asm"
