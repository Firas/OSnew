
INCLUDE "../../Macro.asm"
INCLUDE "MemoryPlan.asm"

SECTION '.text' code readable executable
ORG 7E00h
USE16
public loaderStart
loaderStart:
getBootDriveParam:
	mov	[BootDriveNum],	dl
	mov	ah,	48h
	lds	si,	DWORD [DriveParamAddr]
	mov	WORD [si],	42h
	int	13h
	jnc	getSvgaInfo
	
	mov	si,	.errorTip
	jmp	printError16
.errorTip:	db	'An error occurs when getting the parameters of the boot drive', 0

BootDriveNum:	db	80h
DriveParamAddr:	dw	0, DRIVE_PARAM_ADDR/16


getSvgaInfo:
	xor	ax,	ax
	mov	ds,	ax
	les	di,	DWORD [SvgaInfoAddr]
	mov	DWORD [es:di],	'VBE2'
	mov	ah,	4Fh
	int	10h
	cmp	ax,	4Fh
	je	queryMemory
	
	mov	si,	.errorTip
	jmp	printError16
.errorTip:	db	'An error occurs when getting the information of SuperVGA', 0

SvgaInfoAddr:	dw	0, SVGA_INFO/16

INCLUDE "queryMemory.asm"	
INCLUDE "protectedMode32.asm"

USE32

DISPLAY_OFFSET	endLoader
