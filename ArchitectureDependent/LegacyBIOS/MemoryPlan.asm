
IF ~DEFINED _MEMORY_PLAN_H_
_MEMORY_PLAN_H_	EQU	1

CPUID_ADDR	EQU	18000h

; Int 13/AH=48h: http://www.ctyme.com/intr/rb-0715.htm
; 65-byte buffer for drive parameters
DRIVE_PARAM_ADDR	EQU	18180h

; Int 10/AX=4F01h: http://www.ctyme.com/intr/rb-0274.htm
; 256-byte buffer for mode information
SVGA_MODE_INFO	EQU	18200h
; Int 10/AX=4F00h: http://www.ctyme.com/intr/rb-0273.htm
; 512-byte buffer for SuperVGA information
SVGA_INFO	EQU	18300h

MEMORY_STRUCTURE	EQU	18800h
KERNEL_MEMORY_RECORD0	EQU	19000h

IDT_ADDR	EQU	1A000h

PAGE_STRUC1	EQU	1B000h
PAGE_STRUC2	EQU	1C000h
PAGE_STRUC3	EQU	1D000h
PAGE_STRUC4	EQU	1E000h


MEM_MAP_ADDR	EQU	20000h

END IF ; _MEMORY_PLAN_H_
