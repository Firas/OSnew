; FAT32活动分区引导记录

; FAT32 Volumn Boot Record

INCLUDE "../../Macro.asm"

ORG 7C00h
USE16
VbrFat32:
	jmp	CodeStart
	nop
.version:	db	'MSDOS5.0'
.bytesSector:	dw	512
.sectorsCluster:	db	1
.reserveSector:	dw	2
.FATs:		db	2
.rootItems:	dw	0
.sectorsOld:	dw	0
.mediaDescriptor:	db	0F8h
.sectorsFatOld:	dw	0
.sectorsTrack:	dw	0
.heads:		dw	0
.hiddenSectors:	dd	0
.sectors:	dd	0
.sectorsFat:	dd	0
.extendedFlag:	dw	0
.FatVersion:	dw	0
.firstCluster:	dd	0
.infoSector:	dw	1
.backupBoot:	dw	0
.reserved:	db	12 dup(0)
.drive:		db	80h
.track:		db	0
.signature:	db	29h
.serialNumber:	dd	0
.label:		db	'FirasOS    '
.fileSystem:	db	'FAT32   '

DISPLAY_OFFSET	CodeStart
	mov	eax,	[VbrFat32.hiddenSectors]
	add	eax,	9
	jmp	far 0:readOtherLoader
	
INCLUDE	"loadLoader.asm"
