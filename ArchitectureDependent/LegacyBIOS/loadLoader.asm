readOtherLoader:
.setStack:
	cli
	lss	sp,	DWORD [.stackAddr]
	lds	si,	DWORD [.addrPacketAddr]
	les	bx,	DWORD [.bxAddr]
	sti
	jmp	.saveDriveNum
.stackAddr:	dw	7C00h, 0
.addrPacketAddr:	dw	.addrPacket, 0
.bxAddr:	dw	55AAh, 0

.addrPacket:	dw	10h, 16, 7E00h, 0
.sectorNum:	dq	0

.driveNum:	db	80h

.saveDriveNum:
	mov	[.driveNum],	dl
	mov	DWORD [.sectorNum],	eax
.clearScreen:
	mov	ax,	3	; set video mode
	int	10h
	mov	ah,	2	; set cursor position
	xor	dx,	dx	; top left
	mov	bh,	dh	; page 0
	int	10h
	
.readDiskForOtherLoader:
	mov	ah,	42h
	mov	dl,	[.driveNum]
	int	13h
	jnc	loaderStart
	
DiskReadError:
	mov	si,	.tip
	jmp	printError16
.tip:	db	'An error occurs when reading disk', 0

INCLUDE "printError16.asm"

Blank:	db	(BootSignature-Blank) dup(0)
ORG 7DFEh
BootSignature:	dw	0AA55h

loaderStart:
