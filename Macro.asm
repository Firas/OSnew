IF ~DEFINED _MACRO_H_

MACRO	DISPLAY_OFFSET	IDENTIFIER{
	LOCAL	d
	DISPLAY 'Offset of '
	DISPLAY `IDENTIFIER
	DISPLAY ' is 0x'
	
	IDENTIFIER:
	REPEAT 64/4
		d = '0' + $ shr (64-%*4) and 0Fh
		IF d > '9'
			d = d + 'A'-'9'-1
		END IF
		DISPLAY d
	END REPEAT
	DISPLAY 13,10
}

END IF	; _MACRO_H_
